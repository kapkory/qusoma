<?php
$controller = "AddressController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeShippingAddress');
Route::get('/list',$controller.'listShippingAddresses');
Route::delete('/delete/{shippingaddress}',$controller.'destroyShippingAddress');