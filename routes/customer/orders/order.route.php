<?php
$controller = "OrderController@";
Route::delete('delete/{order}',$controller.'destroyOrder');
Route::post('update',$controller.'updateQuantity');
Route::get('{order_id}',$controller.'orderItems');
Route::get('{order_id}/list',$controller.'listOrderBooks');
