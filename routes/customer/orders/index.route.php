<?php
$controller = "OrderController@";
$controller1 = "PaymentController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeOrder');
Route::get('/list',$controller.'listOrders');


Route::get('choose-payment/{order_id}',$controller1.'index');
Route::get('checkout/{method}/{order_id}',$controller1.'checkout');
Route::get('paypal-checkout/{order_id}',$controller1.'approveCheckout');
Route::get('payment/{order_id}',$controller1.'showPaymentPage');
Route::post('order/{order_id}/pay-order',$controller1.'payOrder');
