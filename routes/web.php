<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\IndexController@index');
Route::get('contact-us', 'Frontend\Menu\IndexController@contactUs');
Route::post('contact-us', 'Frontend\Menu\IndexController@saveContacts');
Route::get('about-us', 'Frontend\Menu\IndexController@aboutUs');
Route::get('blogs', 'Frontend\Menu\IndexController@blogs');
Route::get('book-list', 'Frontend\Products\IndexController@index');
Route::post('customer-login','Auth\UserController@login');
Route::post('customer-register','Auth\UserController@register');
//Route::get('books-in-nairobi', 'Frontend\Products\IndexController@books');
Route::get('book/{slug}', 'Frontend\Books\BookController@index');
Route::get('book-basket', 'Frontend\Books\CartController@index');
Route::post('book/{slug}', 'Frontend\Books\BookController@checkoutMpesa');
Route::get('checkout', 'Frontend\Checkout\CheckoutController@index');

Route::get('book-category/{slug}', 'Frontend\Books\CategoryController@index');


Route::get('search','Frontend\SearchController@index');

Route::get('blog/{slug}', 'Frontend\Menu\BlogController@index');


Route::get('gallery', 'Frontend\Books\BooksController@gallery');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware'=>['auth','web','member']],function(){

    Route::get('home','HomeController@index')->middleware('verified');
    $routes_path = base_path('routes');
    $route_files = File::allFiles(base_path('routes'));
    foreach($route_files as $file){
        $path = $file->getPath();
        if($path != $routes_path){
            $file_name = $file->getFileName();
            $prefix = str_replace($file_name,'',$path);
            $prefix = str_replace($routes_path,'',$prefix);
            $file_path = $file->getPathName();
            $this->route_path = $file_path;
            $arr = explode('/',$prefix);
            $len = count($arr);
            $main_file= $arr[$len-1];
            $arr = array_map('ucwords',$arr);
            $arr = array_filter($arr);
            $ext_route = str_replace('index.route.php','',$file_name);
            $ext_route = str_replace($main_file,'',$ext_route);
            $ext_route = str_replace('.route.php','',$ext_route);
            $ext_route = str_replace('web','',$ext_route);
            if($ext_route)
                $ext_route = '/'.$ext_route;
            Route::group(['namespace'=>implode('\\',$arr),'prefix'=>strtolower($prefix.$ext_route)],function(){
                require $this->route_path;
            });
        }
    }
});
