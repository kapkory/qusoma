<?php
$controller = "ContactController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeContact');
Route::post('mark-read/{contact_id}',$controller.'markRead');
Route::get('/list/{status}',$controller.'listContacts');
Route::delete('/delete/{contact}',$controller.'destroyContact');
