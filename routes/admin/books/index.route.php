<?php
$controller = "BooksController@";
Route::get('/',$controller.'index');
Route::get('/list/{categories}',$controller.'listBooks');
Route::delete('/delete/{book}',$controller.'destroyBook');
