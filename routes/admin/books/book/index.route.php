<?php
$controller = "BookController@";
Route::get('create',$controller.'create');
Route::post('create',$controller.'storeBook');
Route::get('/{slug}',$controller.'index');
Route::post('/{book_id}/assign-category',$controller.'assignCategory');
