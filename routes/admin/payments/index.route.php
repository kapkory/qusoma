<?php
$controller = "PaymentsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storePayment');
Route::get('/list',$controller.'listPayments');
Route::delete('/delete/{payment}',$controller.'destroyPayment');