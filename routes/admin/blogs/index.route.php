<?php
$controller = "BlogController@";
Route::get('/',$controller.'index');
Route::get('create',$controller.'create');
Route::get('blog/{blog_id}',$controller.'viewBlog');
Route::post('activate/{blog_id}',$controller.'activateBlog');
Route::post('deactivate/{blog_id}',$controller.'deactivateBlog');
Route::post('create',$controller.'storeBlog');
Route::post('upload-image',$controller.'uploadImage');
Route::get('/list',$controller.'listBlogs');
Route::delete('/delete/{blog}',$controller.'destroyBlog');
