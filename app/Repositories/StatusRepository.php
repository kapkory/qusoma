<?php

namespace App\Repositories;


class StatusRepository
{
    //withdrawal account :default account ni two
    public static function getBookStatus($state)
    {
        $statuses = [
            'pending' => 0,
            'active' => 1,
            'sold' => 2,
            'borrowed' => 3,
        ];
        if (is_numeric($state))
            $statuses = array_flip($statuses);

        return self::checkState($state,$statuses);
    }

    public static function getAssignStatus($state, $status = null)
    {
        $statuses = [
            'active' => 1,
            'complete' => 2,
            'approved' => 3,
            'rejected' => 4,
            'cancelled' => 5,
            'revision' => 6,
            'disputed' => 7,

        ];
        if ($status)
            return $statuses;


        if (is_numeric($state))
            $statuses = array_flip($statuses);

        return self::checkState($state,$statuses);
    }

    public static function getUserStatus($state)
    {
        $statuses = [
            'inactive' => 0,
            'active' => 1,
            'suspended' => 2,
            'deactivated' => 3,
            'rejected' => 4
        ];
        if (is_numeric($state))
            $statuses = array_flip($statuses);

        return self::checkState($state,$statuses);
    }


    public static function getOrderStatus($state){
        $statuses = [
            'pending'=>0,
            'processing'=>1, //payment in progress
            'paid'=>2,  //paid and has not been shipped
            'completed'=>3, //paid and shipped
            'cancelled'=>4
        ];
        return self::checkState($state,$statuses);
    }

    public static function getPaymentStatus($state){
        $statuses = [
            'pending'=>0,
            'successful'=>1,
            'cancelled'=>2
        ];
        return self::checkState($state,$statuses);
    }

    public static function getWithdrawalStatus($state){
        $statuses = [
            'pending'=>0,
            'processed'=>1,
            'cancelled'=>2
        ];
        return self::checkState($state,$statuses);
    }

    public static function getDisputeStatus($state){
        $statuses = [
            'active'=>1,
            'resolved'=>2
        ];
        return self::checkState($state,$statuses);
    }

    public static function checkState($state,$statuses){
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }

            return $states;
        }
        return $statuses[$state];
    }

}
