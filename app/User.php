<?php

namespace App;

use App\Models\Core\Order;
use App\Models\Core\ShippingAddress;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function lastDeliveryAddress(){
        $address = ShippingAddress::join('orders','orders.shipping_address_id','=','shipping_addresses.id')
            ->where('orders.customer_id',auth()->id())
            ->where('orders.status',0)
            ->orderBy('orders.id', 'DESC')
            ->select('shipping_addresses.*')
            ->first();
        if (!$address)
            return false;
        return $address;
    }
}
