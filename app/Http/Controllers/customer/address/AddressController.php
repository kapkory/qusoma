<?php

namespace App\Http\Controllers\customer\address;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\ShippingAddress;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class AddressController extends Controller
{
            /**
         * return shippingaddress's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store shippingaddress
     */
    public function storeShippingAddress(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('shippingaddresses', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return shippingaddress values
     */
    public function listShippingAddresses(){
        $shippingaddresses = ShippingAddress::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $shippingaddresses->get();
        return SearchRepo::of($shippingaddresses)
            ->addColumn('action',function($shippingaddress){
                $str = '';
                $json = json_encode($shippingaddress);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'shippingaddress_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
            //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/shippingaddresses/delete').'\',\''.$shippingaddress->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete shippingaddress
     */
    public function destroyShippingAddress($shippingaddress_id)
    {
        $shippingaddress = ShippingAddress::findOrFail($shippingaddress_id);
        $shippingaddress->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'ShippingAddress deleted successfully']);
    }
}
