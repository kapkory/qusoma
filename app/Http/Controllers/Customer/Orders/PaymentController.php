<?php

namespace App\Http\Controllers\Customer\Orders;

use App\Models\Core\Order;
use App\Models\Core\OrderBook;
use App\Models\Core\Payment;
use App\Repositories\MpesaRepository;
use App\Repositories\PaypalRepository;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function index($order_id){

        return view($this->folder.'choose_payment',compact('order_id'));
    }

    public function checkout($payment_method,$order_id){
        $order = Order::findOrFail($order_id);
        if ($order->status == StatusRepository::getOrderStatus('paid'))
            return redirect('customer/orders/order/'.$order_id);
        $orders = OrderBook::join('books','order_books.book_id','=','books.id')
            ->where([
//                ['orders.customer_id','=',auth()->id()],
                ['order_books.order_id',$order_id]
            ])->select('order_books.*','books.title','books.slug','books.image','books.price')->get();

//        dd($orders);
        return view($this->folder.'all_checkout',compact('payment_method','orders','order_id'));
    }

    public function payOrder($order_id){
        $order = Order::findOrFail($order_id);
        $payment = new Payment();
        $payment->user_id = auth()->id();
        $payment->order_id = $order_id;
        $payment->amount = $order->cost;
        $payment->save();

      if (\request('method') == 'mpesa'){
          $mpesaRepository= new MpesaRepository();
         $response =  $mpesaRepository->stkPush($payment->id);
         $responses = json_decode($response,true);
         if (isset($responses['ResponseCode']) && $responses['ResponseCode']== 0){
             $checkoutRequestId = $responses['CheckoutRequestID'];
             $payment->transaction_reference = $checkoutRequestId;
             $payment->save();

             $order = Order::findOrFail($order_id);
             $order->status = StatusRepository::getOrderStatus('processing');
             $order->save();

             return redirect('customer/orders/order/'.$order_id.'?payment=success');
         }
      }
      elseif((\request('method') == 'paypal')){
          $cancel_url = url()->current();
          $paypal = new PaypalRepository("customer/orders/checkout/paypal/$order_id","customer/orders/paypal-checkout/$order_id");

          return $paypal->checkout($order);
      }
       return redirect()->back();
    }

    public function showPaymentPage($order_id){
        $method = \request('method');
        return view($this->folder.'payment',compact('order_id','method'));
    }
    public function approveCheckout($id)
    {
        $paypal = new PaypalRepository("customer/orders/checkout/paypal/$id", "customer/orders/paypal-checkout/$id");
        $token = request('token');
        $payerID = request('PayerID');
        $paymentId = request('paymentId');
        $data = $paypal->execute($payerID, $paymentId);
        $transaction_code = $data->id;
        $transaction_fee = $data->transactions[0]->related_resources[0]->sale->transaction_fee->value;
        $amount = $data->transactions[0]->amount->total;
        $paid_order_id = $data->transactions[0]->item_list->items[0]->sku;

        $payment= Payment::where('order_id',$paid_order_id)->orderBy('created_at','Desc')->first();
        $payment->transaction_reference = $transaction_code;
        $payment->save();

        $order = Order::findOrFail($paid_order_id);
        $order->status = StatusRepository::getOrderStatus('paid');
        $order->save();
        return redirect('customer/orders/order/'.$paid_order_id.'?payment=success');

    }
}
