<?php

namespace App\Http\Controllers\Customer\Orders;

use App\Models\Core\OrderBook;
use App\Models\Core\ShippingAddress;
use App\Repositories\StatusRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\Order;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class OrderController extends Controller
{
            /**
         * return order's index view
         */
    public function index(){
        $ordered_books = Order::where('customer_id',auth()->id())->count();
        return view($this->folder.'index',compact('ordered_books'));
    }

    public function orderItems($order_id){
        $order = Order::findOrFail($order_id);
        return view($this->folder.'view_items',compact('order'));
    }

    public function updateQuantity(){
        \request()->validate($this->getValidationFields(['quantity']));
        $orderBook = OrderBook::findOrFail(\request('id'));
        $orderBook->quantity = \request('quantity');
        $orderBook->save();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Quantity Updated']);
    }

    public function listOrderBooks($order_id){
        $orders = OrderBook::join('orders','order_books.order_id','=','orders.id')
        ->join('books','order_books.book_id','=','books.id')
            ->where([
                ['orders.customer_id','=',auth()->id()],
                ['order_books.order_id','=',$order_id]
            ])->select('order_books.*','books.title as title','orders.status as order_status');
        if(\request('all'))
            return $orders->get();
        return SearchRepo::of($orders)
            ->addColumn('action',function($order){
                $str = '';
                $json = json_encode($order);
               if ($order->order_status == StatusRepository::getOrderStatus('pending'))
                 {
                   $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'book_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                   $str.='&nbsp;<a href="#" onclick="deleteItem(\''.url('customer/orders/order/delete').'\',\''.$order->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                 }
               elseif($order->order_status == StatusRepository::getOrderStatus('processing'))
                   $str.='<label class="badge badge-info">Payment: Processing</label>';
               elseif($order->order_status == StatusRepository::getOrderStatus('paid'))
                   $str.='<label class="badge badge-success">Payment: Paid</label>';
               else
                   $str.='<label class="badge badge-warning">Payment: Cancelled</label>';


                return $str;
            })->make();
    }

    /**
     * store order
     */
    public function storeOrder(){
        if (\request('method') == 1)
        request()->validate(['pickup_date']);
        else
            request()->validate(['address_1','postal_address','town']);


        if (\request('method') == 2){
//            "county_id","address_1","address_2","postal_code","town"
            $addr = new ShippingAddress();
            $addr->county_id = \request('county');
            $addr->address_1 = \request('address_1');
            $addr->address_2 = \request('address_2');
            $addr->postal_code = \request('postal_address');
            $addr->town = \request('town');
            $addr->save();
            $address_id = $addr->id;
            $pickup_date = null;
        }else
        {
            $pickup_date = Carbon::parse(\request('pickup_date'))->toDateString();
            $address_id =null;
        }


        $books = json_decode(\request('books'),true);
        if (is_string($books))
            $books = json_decode($books);

        $ord = new Order();
        $ord->customer_id = auth()->id();
        $ord->shipping_address_id = $address_id;
        $ord->pickup_date = $pickup_date;
        $ord->save();
        $order_id = $ord->id;
        $cost = 0;
        foreach ($books as $book){
            $cost += $book['quantity'] * $book['price'];
            $ord_book = new OrderBook();
            $ord_book->order_id = $order_id;
            $ord_book->book_id = $book['id'];
            $ord_book->quantity = $book['quantity'];
            $ord_book->unit_price = $book['price'];
            $ord_book->save();
        }
        $ord->cost = $cost;
        $ord->save();
//
        return ['status'=>true,'redirect'=>url('customer/orders/choose-payment/'.$order_id)];
    }

    /**
     * return order values
     */
    public function listOrders(){
//        join('books','orders.book_id','=','books.id')
//            ->
        $orders = Order::where([
            ['customer_id','=',auth()->id()],
            ['status','!=',StatusRepository::getOrderStatus('paid')]
        ])->select('orders.*',"orders.shipping_address_id as shipping_address");
        if(\request('all'))
            return $orders->get();
        return SearchRepo::of($orders)
            ->addColumn('Id',function($order){
                return '<a href="'.url("customer/orders/order/".$order->id).'">#order'.$order->id.'</a>';
            })
            ->addColumn('cost',function($order){
                return '<span style="color: darkgreen;font-size: 23px" class="font-600">Ksh '.number_format($order->cost,2).'</span>';
            })
            ->addColumn('pickup_date',function($order){
                if (!$order->pickup_date)
                return 'Shipping';
                return $order->pickup_date;
            })
            ->addColumn('shipping_address',function($order){
                if (!$order->shipping_address)
                return '<span style="font-size: 15px" class="text-info">Pick in Town</span>';
                else{
                    $shipping = ShippingAddress::findOrFail($order->shipping_address);
                    return '<div class="alert alert-info">Town: '.$shipping->town.';
                                <br>Postal Code: '.$shipping->postal_code.' <br> Address Line : '.$shipping->address_1.', '.$shipping->address_2.'</div>';
                }
            })
            ->addColumn('action',function($order){
                $str = '';
                $json = json_encode($order);
                $str.='<a href="'.url("customer/orders/choose-payment/".$order->id).'"  class="btn badge btn-info btn-sm"><i class="fa fa-eye"></i> Pay</a>';
//                $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url('customer/orders/delete').'\',\''.$order->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Remove</a>';
                return $str;
            })->make();
    }

    /**
     * delete order
     */
    public function destroyOrder($order_id)
    {
        $order = OrderBook::findOrFail($order_id);
        $order->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Order deleted successfully']);
    }
}
