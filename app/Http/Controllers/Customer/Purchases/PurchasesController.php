<?php

namespace App\Http\Controllers\Customer\Purchases;

use App\Models\Core\Order;
use App\Models\Core\ShippingAddress;
use App\Repositories\SearchRepo;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PurchasesController extends Controller
{
    public function index(){
        return view($this->folder.'index');
    }
    /**
     * return order values
     */
    public function listPurchases(){
        $orders = Order::where([
            ['customer_id','=',auth()->id()],
            ['status','=',StatusRepository::getOrderStatus('paid')]
        ])->select('orders.*',"orders.shipping_address_id as shipping_address");
        if(\request('all'))
            return $orders->get();
        return SearchRepo::of($orders)
            ->addColumn('Id',function($order){
                return '<a href="'.url("customer/orders/order/".$order->id).'">#order'.$order->id.'</a>';
            })
            ->addColumn('cost',function($order){
                return '<span style="color: darkgreen;font-size: 23px" class="font-600">Ksh '.number_format($order->cost,2).'</span>';
            })
            ->addColumn('pickup_date',function($order){
                if (!$order->pickup_date)
                    return 'Shipping';
                return $order->pickup_date;
            })
            ->addColumn('shipping_address',function($order){
                if (!$order->shipping_address)
                    return '<span style="font-size: 15px" class="text-info">Pick in Town</span>';
                else{
                    $shipping = ShippingAddress::findOrFail($order->shipping_address);
                    return '<div class="alert alert-info">Town: '.$shipping->town.';
                                <br>Postal Code: '.$shipping->postal_code.' <br> Address Line : '.$shipping->address_1.', '.$shipping->address_2.'</div>';
                }
            })
            ->addColumn('action',function($order){
                $str = '';
                $json = json_encode($order);
                $str.='<a href="'.url("customer/orders/order/".$order->id).'"  class="btn badge btn-info btn-sm"><i class="fa fa-eye"></i> View</a>';
                return $str;
            })->make();
    }
}
