<?php

namespace App\Http\Controllers\Admin\Payments;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\Payment;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class PaymentsController extends Controller
{
            /**
         * return payment's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store payment
     */
    public function storePayment(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('payments', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return payment values
     */
    public function listPayments(){
        $payments = Payment::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $payments->get();
        return SearchRepo::of($payments)
            ->addColumn('action',function($payment){
                $str = '';
                $json = json_encode($payment);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'payment_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
            //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/payments/delete').'\',\''.$payment->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete payment
     */
    public function destroyPayment($payment_id)
    {
        $payment = Payment::findOrFail($payment_id);
        $payment->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Payment deleted successfully']);
    }
}
