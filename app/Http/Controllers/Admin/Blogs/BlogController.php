<?php

namespace App\Http\Controllers\Admin\Blogs;

use App\Models\Core\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\Blog;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BlogController extends Controller
{
            /**
         * return blog's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    public function viewBlog($blog_id){
        $blog = Blog::findOrFail($blog_id);
        return view($this->folder.'preview_blog',compact('blog'));
    }
    public function activateBlog($id){
        $blog = Blog::findOrFail($id);
        $blog->status = 1;
        $blog->save();
        return redirect()->back()->with('notice',['type'=>'success','message'=>$blog->title.' has been successfully activated']);
    }

    public function deactivateBlog($id){
        $blog = Blog::findOrFail($id);
        $blog->status = 0;
        $blog->save();
        return redirect()->back()->with('notice',['type'=>'success','message'=>$blog->title.' has been successfully deactivated']);
    }
    public function create(){
        return view($this->folder.'create',[

        ]);
    }

    /**
     * store blog
     */
    public function storeBlog(){
        request()->validate($this->getValidationFields(['title','description']));
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('blogs', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $data['slug'] = Str::slug($data['title']);
        $data['image'] = null;
        if (\request('image')){
         $result = $this->uploadImage('image');
            $data['image'] = $result['path'];
        }
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice',['type'=>'success','message'=>"Blog has been created successfully"]);
    }

    public function uploadImage($image = null){
        if ($image)
            $file = \request('image');
        else
            $file = \request('upload');

        $originan_name = $file->getClientOriginalName();
        $file_type = $file->getClientMimeType();
        $file_size = $file->getSize();
//            dd($file_size);
        $arr = explode('.',$originan_name);
        $ext = $arr[count($arr)-1];
        $file_name = Str::slug(str_replace($ext,'',$originan_name)).'.'.$ext;

        $path = '/blogs';

        $new_path = 'public/'.$path;
        $public_path = '/storage'.$path;
        $path = '/public'.$path;

        $new_name = Str::random(3).'_'.date('H_i_s').'_'.$file_name;

        $disk = 'local';

        Storage::disk($disk)->putFileAs($new_path,$file,$new_name);
        $image = new Image();
        $image->name = $originan_name;
        $image->size = $file_size;
        $image->path = $public_path.'/'.$new_name;
        $image->type = $file_type;
        $image->user_id = auth()->id();
        $image->save();

        $url = $public_path.'/'.$new_name;
        return [
            "uploaded"=>true,
            "fileName"=>$originan_name,
            "path"=>$url,
            "url"=>url($url)
        ];
    }

    /**
     * return blog values
     */
    public function listBlogs(){
        $blogs = Blog::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $blogs->get();
        return SearchRepo::of($blogs)
            ->addColumn('description',function($blog){
                return substr(strip_tags($blog->description), 0, 100);
            })
            ->addColumn('action',function($blog){
                $str = '';
                $json = json_encode($blog);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'blog_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/blogs/delete').'\',\''.$blog->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                $str.='<a href="'.url("admin/blogs/blog/".$blog->id).'"  class="btn btn-success badge btn-xs m-1"><i class="fa fa-eye"></i> View</a>';
                return $str;
            })->make();
    }

    /**
     * delete blog
     */
    public function destroyBlog($blog_id)
    {
        $blog = Blog::findOrFail($blog_id);
        $blog->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Blog deleted successfully']);
    }
}
