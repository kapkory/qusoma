<?php

namespace App\Http\Controllers\Admin\Books\Book;

use App\Models\Core\Book;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class BookController extends Controller
{
    public function index($slug){
        $book = Book::where('slug',$slug)->first();
        return view($this->folder.'index',compact('book'));
    }

    public function create(){
        return view($this->folder.'create');
    }
    public function assignCategory($book_id){
        request()->validate($this->getValidationFields(['category']));

        $book = Book::findOrFail($book_id)->update(['category_id'=>\request('category')]);
        return back()->with('notice',['type'=>'success',"message"=>"Book has been assigned a category"]);
    }

    public function markSold($book_id){
        $book = Book::findOrFail($book_id)->update(['status'=>StatusRepository::getBookStatus('sold')]);
        return back()->with('notice',['type'=>'success',"message"=>"Book has been marked as sold"]);
    }

    /**
     * store book
     */
    public function storeBook(){
        request()->validate($this->getValidationFields(['title','description','price','author','category_id','image']));
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('books', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if (!isset($data['id'])){
            $slug = Str::slug($data['title']).'-'.Str::random(9);
            $data['slug'] = strtolower($slug);
            $data['seo_title'] = ($data['seo_title']) ? $data['seo_title'] : 'Buy '.$data['title'].' at Qusoma Library Services, the leading bookshop in Nairobi';
            $data['seo_description'] = ($data['seo_description']) ? $data['seo_description'] : $data['description'];
            $data['format'] = ($data['format']) ? $data['format'] : 'paperback';

            if (isset($data['image'])){
                $image = request()->file('image');
                $ext = $image->getClientOriginalExtension();
                $name = $data['slug']. "." . $ext;
                $image->move(storage_path() . '/app/public/books/images', $name);
                $data['image'] = 'books/images/'.$name;
            }
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }
}
