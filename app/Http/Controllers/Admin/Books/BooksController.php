<?php

namespace App\Http\Controllers\Admin\Books;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\Book;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class BooksController extends Controller
{
            /**
         * return book's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }



    /**
     * return book values
     */
    public function listBooks($category){
        if ($category == 'with_category')
            $books = Book::whereNotNull('category_id');
        else
            $books = Book::whereNull('category_id');

        if(\request('all'))
            return $books->get();
        return SearchRepo::of($books)
            ->addColumn('image',function($book){
             return '<img height="40" src="'.url($book->image).'">';
            })
            ->addColumn('description',function($blog){
                return substr(strip_tags($blog->description), 0, 100);
            })
            ->addColumn('action',function($book){
                $str = '';
                $json = json_encode($book);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'book_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                $str.='&nbsp;&nbsp;<a href="'.url("admin/books/book/".$book->slug).'" class="btn badge btn-success btn-sm m-1"><i class="fa fa-eye"></i> View</a>';
            //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/books/delete').'\',\''.$book->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete book
     */
    public function destroyBook($book_id)
    {
        $book = Book::findOrFail($book_id);
        $book->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Book deleted successfully']);
    }
}
