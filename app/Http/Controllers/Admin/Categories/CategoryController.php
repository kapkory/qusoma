<?php

namespace App\Http\Controllers\Admin\Categories;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\Category;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
            /**
         * return category's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store category
     */
    public function storeCategory(){

        request()->validate($this->getValidationFields(['name']));
        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        if (isset($data['id'])){
            $data['slug'] = strtolower(Str::slug($data['slug']));
            $this->autoSaveModel($data);
            return redirect()->back();
        }
        else{
            $data['parent_category'] = 2;
            $data['is_sub'] = 1;
        }
        $data['slug'] = strtolower(Str::slug($data['slug']));
        $data['meta_title'] = ($data['meta_title']) ? $data['meta_title'] : "Buy ".$data['name'].' books at Qusoma Library';
        $data['meta_description'] = ($data['meta_description']) ? $data['meta_description'] : "Buy ".$data['name'].' books at Qusoma Library Services, , The leading bookshop in Nairobi, We do deliveries to other parts of the country';
        $data['description'] = ($data['description']) ? $data['description'] : "Buy ".$data['name'].' books at Qusoma Library Services, The leading bookshop in Nairobi, We do deliveries to other parts of the country';
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return category values
     */
    public function listCategories(){
        $categories = Category::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $categories->get();
        return SearchRepo::of($categories)
            ->addColumn('action',function($category){
                $str = '';
                $json = json_encode($category);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'category_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
            //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/categories/delete').'\',\''.$category->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete category
     */
    public function destroyCategory($category_id)
    {
        $category = Category::findOrFail($category_id);
        $category->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Category deleted successfully']);
    }
}
