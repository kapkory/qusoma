<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Models\Core\Order;
use App\Models\Core\ShippingAddress;
use App\Repositories\SearchRepo;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function index(){
        return view($this->folder.'index');
    }
    /**
     * return order values
     */
    public function listOrders($status){
        switch ($status){
            case 'unpaid':
                $orders = Order::where([
                    ['status','=',StatusRepository::getOrderStatus(['pending','processing'])]
                ])->select('orders.*',"orders.shipping_address_id as shipping_address");
                break;
            case 'paid_undelivered' :
                $orders = Order::where([
                    ['status','=',StatusRepository::getOrderStatus('paid')]
                ])->select('orders.*',"orders.shipping_address_id as shipping_address");
                break;
            case 'paid_delivered' :
                $orders = Order::where([
                    ['status','=',StatusRepository::getOrderStatus('completed')]
                ])->select('orders.*',"orders.shipping_address_id as shipping_address");
                break;
        }

        if(\request('all'))
            return $orders->get();
        return SearchRepo::of($orders)
            ->addColumn('Id',function($order){
                return '<a href="'.url("admin/orders/order/".$order->id).'">#order'.$order->id.'</a>';
            })
            ->addColumn('cost',function($order){
                return '<span style="color: darkgreen;font-size: 23px" class="font-600">Ksh '.number_format($order->cost,2).'</span>';
            })
            ->addColumn('pickup_date',function($order){
                if (!$order->pickup_date)
                    return 'Shipping';
                return $order->pickup_date;
            })
            ->addColumn('shipping_address',function($order){
                if (!$order->shipping_address)
                    return '<span style="font-size: 15px" class="text-info">Pick in Town</span>';
                else{
                    $shipping = ShippingAddress::findOrFail($order->shipping_address);
                    return '<div class="alert alert-info">Town: '.$shipping->town.';
                                <br>Postal Code: '.$shipping->postal_code.' <br> Address Line : '.$shipping->address_1.', '.$shipping->address_2.'</div>';
                }
            })
            ->addColumn('action',function($order){
                $str = '';
                $json = json_encode($order);
                $str.='<a href="'.url("admin/orders/order/".$order->id).'"  class="btn badge btn-info btn-sm"><i class="fa fa-eye"></i> View</a>';
//                $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url('customer/orders/delete').'\',\''.$order->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Remove</a>';
                return $str;
            })->make();
    }
}
