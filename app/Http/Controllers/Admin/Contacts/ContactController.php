<?php

namespace App\Http\Controllers\Admin\Contacts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\Contact;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class ContactController extends Controller
{
            /**
         * return contact's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store contact
     */
    public function storeContact(){
        request()->validate($this->getValidationFields(['name','message']));
        $data = \request()->all();
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return contact values
     */
    public function listContacts($status){
        if ($status == 'unread')
        $contacts = Contact::where([
            ['read_at','=',0]
        ]);
        else
            $contacts = Contact::where([
                ['read_at','!=',0]
            ]);
        if(\request('all'))
            return $contacts->get();
        return SearchRepo::of($contacts)
            ->addColumn('action',function($contact){
                $str = '';
                $json = json_encode($contact);
//                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'contact_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                if ($contact->read_at == 0)
                $str.='<a href="#" onclick="runPlainRequest(\''.url("admin/contacts/mark-read/".$contact->id).'\');" class="btn badge btn-info btn-sm"> Mark Read</a>';
            //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/contacts/delete').'\',\''.$contact->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    public function markRead($contact_id){
        $contact = Contact::findOrFail($contact_id);
        $contact->read_at = 1;
        $contact->save();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Successfully Read']);

    }

    /**
     * delete contact
     */
    public function destroyContact($contact_id)
    {
        $contact = Contact::findOrFail($contact_id);
        $contact->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Contact deleted successfully']);
    }
}
