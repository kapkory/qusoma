<?php

namespace App\Http\Controllers\Api;

use App\Models\Core\Book;
use App\Models\Core\Category;
use App\Models\Core\County;
use App\Models\Core\Order;
use App\Models\Core\Payment;
use App\Repositories\StatusRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{
    public function listCategories(){
        return Category::where('name','!=','Books')->select('id','name')->get();
    }

    public function listCounties(){
        return County::select('id','name')->get();
    }

    public function bookList(){
        return Book::inRandomOrder()->paginate(15);
    }
    public function mpesaCallback(){
        header("Content-Type: application/json");
        $payment_id = \request('payment_id');
        $resp =  json_decode(file_get_contents('php://input'));

        //success
        if ($resp->Body->stkCallback->ResultCode == 0){
           $pay = Payment::findOrFail($payment_id);
           $pay->transaction_reference = $resp->Body->stkCallback->CheckoutRequestID;
           $pay->transaction_description = $resp->Body->stkCallback->ResultDesc;
           $pay->status = StatusRepository::getPaymentStatus('successful');
           $pay->save();

           $order = Order::findOrFail($pay->order_id);
           $order->status = StatusRepository::getOrderStatus('paid');
           $order->save();
        }else{
            $pay = Payment::findOrFail($payment_id);
            $pay->transaction_reference = $resp->Body->stkCallback->CheckoutRequestID;
            $pay->transaction_description = $resp->Body->stkCallback->ResultDesc;
            $pay->status = StatusRepository::getPaymentStatus('cancelled');
            $pay->save();
        }
        echo '{"ResultCode": 0, "ResultDesc": "The service was accepted successfully", "ThirdPartyTransID": "1234567890"}';
    }

//    public function getBookById($book_id){
//        $book = Book::findOrFail($book_id)->select('id','title','price','image')->first();
//        return $book;
//    }
}
