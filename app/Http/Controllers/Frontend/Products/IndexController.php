<?php

namespace App\Http\Controllers\Frontend\Products;

use App\Models\Core\Book;
use App\Models\Core\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(){
        $categories = Category::whereHas('books')->get();
        $featured_books = Book::where('featured',1)->inRandomOrder()->paginate(5);

        return view($this->folder.'index',compact('categories','featured_books'));
    }
}
