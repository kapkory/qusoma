<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Core\Blog;
use App\Models\Core\Book;
use App\Models\Core\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(){
        $categories = Category::whereHas('books')->paginate(10);
        $featured_books = Book::where('featured',1)->inRandomOrder()->paginate(3);
        $latest_books = Book::latest()->paginate(4);
        $blogs = Blog::latest()->paginate(4);
        $books  = Book::inRandomOrder()->paginate(6);

        return view($this->folder.'index',compact('categories','featured_books','latest_books','blogs','books'));
    }
}
