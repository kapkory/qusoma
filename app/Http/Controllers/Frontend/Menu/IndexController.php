<?php

namespace App\Http\Controllers\Frontend\Menu;

use App\Models\Core\Blog;
use App\Models\Core\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function contactUs(){
        return view($this->folder.'contact-us');
    }

    public function aboutUs(){
        return view($this->folder.'about-us');
    }

    public function blogs(){
        $blogs = Blog::latest()->paginate(4);
        return view($this->folder.'blogs',compact('blogs'));
    }

    public function saveContacts(){
        $validator = \request()->validate(['name'=>'required','email'=>'required','phone'=>'required','message'=>'required']);
        if (!$validator) {
            return redirect('post/create')
                ->withErrors($validator)
                ->withInput();
        }
        $data = \request()->all();
        Contact::create($data);
        return redirect('contact-us?status=success');
    }
}
