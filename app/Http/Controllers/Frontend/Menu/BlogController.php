<?php

namespace App\Http\Controllers\Frontend\Menu;

use App\Models\Core\Blog;
use App\Models\Core\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index($slug){
        $blog  = Blog::where('slug',$slug)->first();
        $featured_books = Book::where('featured',1)->inRandomOrder()->paginate(3);

        return view($this->folder.'view_blog',compact('blog','featured_books'));
    }
}
