<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Core\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function index(){
        $search = \request('s');
        $books  = Book::where('title','Like','%'.$search.'%')->get();
        if ($books->isEmpty())
            $books  = Book::where('author','Like','%'.$search.'%')->get();

         DB::table('book_searches')->insert([
            'search'=>$search,
            'count'=>count($books),
             'created_at'=>now()
         ]);

        if ($books->isEmpty())
            $books  = Book::where('description','Like','%'.$search.'%')->get();

        $featured_books = Book::where('featured',1)->paginate(6);
        return view($this->folder.'products.search',compact('books','featured_books'));
    }
}
