<?php

namespace App\Http\Controllers\Frontend\Books;

use App\Models\Core\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    public function index($slug){
        $book = Book::where('slug',$slug)->firstOrFail();
        $meta_title = $book->seo_title;
        $meta_description = $book->seo_description;
        if ($book->category_id){
            $related_books = Book::where('category_id',$book->category_id)->paginate(4);
        }
        else
            $related_books = Book::inRandomOrder()->paginate(4);

        return view($this->folder.'view_book',compact('book','meta_title','meta_description','related_books'));
    }

    public function checkoutMpesa($slug){
        $price = 200;
        $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode('GNMVbSGO3o9uqtooSzJpPUQdKdCSGbtI:uCZDMafnnqhGppui');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);

        echo json_decode($curl_response);


        exit;
    }
}
