<?php

namespace App\Http\Controllers\Frontend\Books;

use App\Models\Core\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index($slug){
        $books = Category::where('categories.slug',$slug)
            ->join('books','books.category_id','=','categories.id')
            ->paginate(15);
        $categories = Category::whereHas('books')->get();
        return view($this->folder.'category',compact('books','categories'));
    }
}
