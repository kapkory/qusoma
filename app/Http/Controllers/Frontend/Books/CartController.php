<?php

namespace App\Http\Controllers\Frontend\Books;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function index(){

        return view($this->folder.'view_cart');
    }
}
