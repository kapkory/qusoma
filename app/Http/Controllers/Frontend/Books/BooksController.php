<?php

namespace App\Http\Controllers\Frontend\Books;

use App\Models\Core\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BooksController extends Controller
{
    public function gallery(){
        $books  = Book::paginate(16);
        return view($this->folder.'gallery',compact('books'));
    }
}
