<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(){
        $validatedData = \request()->validate([
            'login_email' => 'required|max:255',
            'login_password' => 'required',
        ]);
        $res = Auth::attempt(['email'=>request('login_email'),'password'=>request('login_password')]);
        if($res){
            return ['status'=>true,'user_id'=>\auth()->id()];
        }
        return ['status'=>false];
    }

    public function register(){
        $validatedData = \request()->validate([
            'email' => 'required|max:255',
            'password' => 'required',
            'name' => 'required',
            'phone' => 'required',
        ]);
        if ($validatedData){
            $user = new User();
            $user->name = \request('name');
            $user->email = \request('email');
            $user->password = bcrypt(\request('password'));
            $user->role = 'customer';
            $user->phone = \request('phone');
            $user->save();
            Auth::login($user);
            return ['status'=>true];
        }
        return ['status'=>false];
    }
}
