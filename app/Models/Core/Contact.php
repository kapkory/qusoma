<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    
	protected $fillable = ["name","email","subject","phone","message","read_at"];

}
