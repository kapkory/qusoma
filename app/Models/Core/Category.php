<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
	protected $fillable = ["name","slug","description","parent_category","is_sub","user_id","meta_title","meta_description","image_url"];

	public function books(){
	    return $this->hasMany(Book::class);
    }
}
