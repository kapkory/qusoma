<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

	protected $fillable = ["title","slug","description","status","user_id","image"];
   public function user(){
       return $this->belongsTo(User::class);
   }
}
