<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{

	protected $fillable = ["county_id","address_1","address_2","postal_code","town"];

}
