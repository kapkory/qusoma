<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
	protected $fillable = ["customer_id","book_id","pickup_date","status"];

}
