<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    
	protected $fillable = ["name","size","path","description","type"];

}
