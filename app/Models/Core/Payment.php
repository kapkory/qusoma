<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    
	protected $fillable = ["order_id","user_id","amount","method","transaction_reference","status"];

}
