<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    
	protected $fillable = ["title","slug","category_id","description","sku","author","image","weight","price","quantity","format","status","seo_title","seo_description","user_id"];
    public function category(){
        return $this->belongsTo(Category::class);
    }
}
