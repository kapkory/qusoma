<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('title');
			$table->string('slug');
			$table->integer('category_id')->nullable();
			$table->longText('description');
			$table->string('sku')->nullable();
			$table->string('author')->nullable();
			$table->string('image')->nullable();
			$table->double('weight')->nullable();
			$table->double('price')->nullable();
			$table->double('quantity')->default(1);
			$table->enum('format',['paperback','audiobook','e-book'])->default('paperback');
			$table->tinyInteger('status')->default(1);
			$table->string('seo_title')->nullable();
			$table->longText('seo_description')->nullable();
			$table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
