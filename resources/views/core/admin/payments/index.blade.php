@extends('layouts.admin')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Payments
@endsection

@section('title') Payments @endsection

@section('content')
<a href="#payment_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD PAYMENT</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","","action"],
    'data_url'=>'admin/payments/list',
    'base_tbl'=>'payments'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'payment_modal',
        'modal_title'=>'PAYMENT FORM',
        'modal_content'=>Form::autoForm(\App\Models\Core\Payment::class,"admin/payments")
    ])
@endsection

<script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".tooltip").tooltip("hide");
 </script>
