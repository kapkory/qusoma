@extends('layouts.admin')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Categories
@endsection

@section('title') Categories @endsection

@section('content')
<a href="#category_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD CATEGORY</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","name","description","meta_title","action"],
    'data_url'=>'admin/categories/list',
    'base_tbl'=>'categories'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'category_modal',
        'modal_title'=>'CATEGORY FORM',
        'modal_content'=>Form::autoForm(["name","slug","description","meta_title","meta_description","form_model"=>\App\Models\Core\Category::class],"admin/categories")
    ])
@endsection
