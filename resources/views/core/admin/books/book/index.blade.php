@extends('layouts.admin')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Book
@endsection

@section('title') Book {{ $book->title }} @endsection

@section('content')
    <div class="row">
      <div class="col-md-8">

          <table class="table table-bordered">
              <tr>
                  <th class="titlecolumn">Book Title</th>
                  <td>{{ @$book->title }}</td>
              </tr>
              <tr>
                  <th class="titlecolumn">Author</th>
                  <td>{{ @$book->author }}</td>
              </tr>
              @if($book->category_id)
                  <tr>
                      <th class="titlecolumn">Category</th>
                      <td>{{ @$book->category->name }}</td>
                  </tr>
              @endif
              <tr>
                  <th class="titlecolumn">Price</th>
                  <td><span style="font-weight: bold" class="text-info">KES {{ @number_format($book->price,2) }}</span></td>
              </tr>

              <tr>
                  <th class="titlecolumn">Quantity</th>
                  <td><b>{{ @$book->quantity }}</b>&nbsp;<small>(copies)</small></td>
              </tr>

              <tr>
                  <th class="titlecolumn">Format</th>
                  <td>{{ @$book->format }}</td>
              </tr>
              <tr>
                  <th class="titlecolumn">Weight</th>
                  <td>{{ @$book->weight }}</td>
              </tr>

              <tr>
                  <th class="titlecolumn">SEO Title</th>
                  <td>{{ @$book->seo_title }}</td>
              </tr>

              <tr>
                  <th class="titlecolumn">Book Description</th>
                  <td>{!! @$book->description !!}</td>
              </tr>

              <tr>
                  <th class="titlecolumn">Meta Description</th>
                  <td>{!! @$book->seo_description !!}</td>
              </tr>
          </table>

      </div>

        <div class="col-md-4">

            <div class="card">
                <div class="card-title">
                    <div class="text-center mt-2">
                        <img class="media-object rounded-circle" alt="64x64" src="{{ url($book->image) }}" style="width: 84px; height: 84px;">
                    </div>
                </div>
                <div class="card-body">
                    @if(!$book->category_id)
                        <button type="button" class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#add_category_modal">Assign Category</button>
                    @endif
                    @if($book->status == \App\Repositories\StatusRepository::getBookStatus('active'))
                            <a href="#" onclick="runPlainRequest(`{{ url('admin/books/book/'.$book->id.'/mark-sold') }}`)" class="btn btn-success btn-sm clear-form m-1" ><i class="fa fa-check"></i> Mark as Sold</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

{{--  Modal Sections  --}}
    @include('common.auto_modal',[
        'modal_id'=>'add_category_modal',
        'modal_title'=>'BOOK FORM',
        'modal_content'=>Form::autoForm(['category','form_model'=>\App\Models\Core\Book::class],'admin/books/book/'.$book->id.'/assign-category')
    ])


    @endsection
@section('scripts')
    <script>
        autoFillSelect('category',"{{ url('api/categories') }}")
    </script>
    @endsection