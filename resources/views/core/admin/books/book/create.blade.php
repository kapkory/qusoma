@extends('layouts.admin')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Book
@endsection

@section('title') Add Book  @endsection

@section('content')
    <div class="row">
        <div class="col-md-10">
        {!! Form::autoForm(["title","category_id","description","price","author","image","weight","sku","quantity","format","seo_title","seo_description","form_model"=>\App\Models\Core\Book::class],"admin/books/book/create") !!}

        </div>
    </div>
    <script>
        autoFillSelect('category_id',"{{ url('api/categories') }}")
    </script>
@endsection