@extends('layouts.admin')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Books
@endsection

@section('title') Books @endsection

@section('content')


<a href="{{ url('admin/books/book/create') }}" class="btn btn-info btn-sm clear-form float-right"><i class="fa fa-plus"></i> ADD BOOK</a>
@include('common.auto_tabs',[
       'tabs'=>['without_category','with_category'],
       'tabs_folder'=>'core.admin.books.tabs',
       'base_url'=>'admin/books/'
   ])

    @include('common.auto_modal',[
        'modal_id'=>'book_modal',
        'modal_title'=>'BOOK FORM',
        'modal_content'=>Form::autoForm(["title","category_id","description","sku","author","image","weight","price","quantity","format","seo_title","seo_description","form_model"=>\App\Models\Core\Book::class],"")
    ])

<script>

    autoFillSelect('category_id',"{{ url('api/categories') }}")
</script>
@endsection

