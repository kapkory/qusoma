@extends('layouts.admin')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Blogs
@endsection

@section('title') Create Blog @endsection
{{--@push('header-scripts')--}}
{{--@endpush--}}
@section('content')

    <div class="col-md-9">
       {!! Form::autoForm(['title',"image",'description','form_model'=>\App\Models\Core\Blog::class],"admin/blogs/create") !!}
    </div>
    <style>
        .ck-editor__editable_inline {
            min-height: 300px;
        }
    </style>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('description', {
                height: 260,
                width: 700,
                filebrowserUploadUrl: "{{ url('admin/blogs/upload-image?_token='.csrf_token()) }}"
            });
        })
    </script>
@endsection
