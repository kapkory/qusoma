@extends('layouts.admin')

@section('title') Blogs @endsection

@section('content')
<a href="{{ url('admin/blogs/create') }}" class="btn btn-info btn-sm clear-form float-right" ><i class="fa fa-plus"></i> ADD BLOG</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","title","description","status","action"],
    'data_url'=>'admin/blogs/list',
    'base_tbl'=>'blogs'
    ])

{{--    @include('common.auto_modal',[--}}
{{--        'modal_id'=>'blog_modal',--}}
{{--        'modal_title'=>'BLOG FORM',--}}
{{--        'modal_content'=>Form::autoForm(\App\Models\Core\Blog::class,"admin/blogs")--}}
{{--    ])--}}
@endsection
