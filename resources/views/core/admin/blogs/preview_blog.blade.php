@extends('layouts.admin')

@section('title')
    #Title :  {{ $blog->title }}
@endsection

@section('content')

    <style>
        h4{
            text-decoration: underline;

        }
    </style>
    <div class="row">
        <div class="col-md-9">
            {!! $blog->description !!}
        </div>
        <div class="col-md-3 pull-right">
            @if($blog->image)
                <span class="text-center">Cover</span>
                <div>
                    <img height="200" src="{{ url($blog->image) }}">
                </div>

            @endif
            @if($blog->status == 0)
                <a href="#" onclick="runPlainRequest(`{{ url('admin/blogs/activate/'.$blog->id) }}`)" class="btn btn-info">Activate</a>
            @else
                <a href="#" onclick="runPlainRequest(`{{ url('admin/blogs/deactivate/'.$blog->id) }}`)" class="btn btn-secondary">Deactivate</a>
            @endif
        </div>


    </div>
@endsection
