@extends('layouts.admin')
@section('content')

    @include('common.auto_tabs',[
       'tabs'=>['unpaid','paid_undelivered','paid_delivered'],
       'tabs_folder'=>'core.admin.orders.tabs',
       'base_url'=>'admin/orders'
   ])

@endsection
