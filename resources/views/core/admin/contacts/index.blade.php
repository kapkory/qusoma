@extends('layouts.admin')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Contacts
@endsection

@section('title') Contacts @endsection

@section('content')

    @include('common.auto_tabs',[
       'tabs'=>['unread','read'],
       'tabs_folder'=>'core.admin.contacts.tabs',
       'base_url'=>'admin/contacts/'
   ])


@endsection
