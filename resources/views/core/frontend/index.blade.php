@extends('layouts.frontend')

@section('content')
    <!--baner slide show-->
    <div class="banner_slide_show mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="categories_menu">
                        <div class="categories_title">
                            <h2 class="categori_toggle"><img src="{{ url('frontend/assets/img/logo/categorie.png') }}" alt=""> All categories</h2>
                        </div>
                        <div class="categories_menu_inner">
                            <ul>
                                @foreach($categories as $category)
                                    <li><a href="{{ url('book-category/'.$category->slug) }}"><i class="fa fa-caret-right"></i> {{ $category->name }}</a></li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-8">
                    <div class="banner_slider">
                        <div class="slider_active owl-carousel">
                            <div class="single_slider" style="background-image: url('{{ url('frontend/assets/img/slider/slider1.jpg') }}')">
                                <div class="row">
                                    <div class="col-lg-6 offset-lg-6 col-md-7 offset-md-5">
                                        <div class="slider_content">
                                            <h1>Elegant <br>Bags </h1>
                                            <div class="slider_desc">
                                                <p>Lorem ipsum dolor sit amet, ex <br> eam mundi populo accusamus, aliquam </p>
                                            </div>
                                            <div class="slider_discount">
                                                <ul>
                                                    <li><i class="fa fa-check"></i> Save up to 10% off</li>
                                                    <li><i class="fa fa-check"></i>  Free Shipping</li>
                                                    <li><i class="fa fa-check"></i>   Start at $99.00</li>
                                                </ul>
                                            </div>
                                            <div class="slider_button">
                                                <a href="index.html#">shop it! </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="single_slider" style="background-image: url('{{ url('frontend/assets/img/slider/slider2.jpg') }}')">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="slider_content sale">
                                            <h2>Tablets </h2>
                                            <h1>Sale </h1>
                                            <div class="slider_desc_up ">
                                                <p>Up to 40 % off on all tablets </p>
                                            </div>
                                            <div class="slider_button">
                                                <a href="index.html#">shop it! </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="top_sellers">
                        <div class="top_title">
                            <h2> Featured Books</h2>
                        </div>
                        <div class="small_product_active owl-carousel">
                            @include('core.frontend.shared.featured_books_sidebar',['featured_books'=>$featured_books])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--baner slide end-->


    <!--shipping area start-->
    <div class="shipping_area mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="single_shipping">
                        <div class="shippin_icone">
                            <i class="fa fa-truck"></i>
                        </div>
                        <div class="shipping_content">
                            <h3>Free shipping on orders over $100</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single_shipping">
                        <div class="shippin_icone">
                            <i class="fa fa-history"></i>
                        </div>
                        <div class="shipping_content">
                            <h3>30-day returns money back guarantee</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single_shipping box3">
                        <div class="shippin_icone">
                            <i class="fa fa-headphones"></i>
                        </div>
                        <div class="shipping_content">
                            <h3>24/7 online support consultations</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--shipping area end-->

    <!--home block section start-->
    <div class="home_block_seciton">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8">

                    <!--featured product area start-->
                    <div class="featured_left mb-40">
                        <div class="top_title">
                            <h2> Shop by Categories</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="hot_category">
                                    <h2>Our Category</h2>
                                    <ul>
{{--                                        <li><a class="active" href="{{ url('books') }}">All Categories</a></li>--}}
                                        @foreach($categories as $category)
                                            <li><a href="{{ url('book-category/'.$category->slug) }}">{{ $category->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="featured_produt" id="category_product">
                                    <div class="featured_active owl-carousel">

                                        @foreach($books as $book)
                                            @if($loop->iteration % 2 != 0)
                                        <div class="single_featured" style="padding: 2px">
                                            @endif

                                            <div class="single_product">
                                                <div class="product_thumb">
                                                    <a href="{{ url('book/'.$book->slug) }}"><img src="{{ url($book->image) }}" alt=""></a>
                                                    <div class="product_discount">
                                                        <span>New</span>
                                                    </div>
                                                    <div class="product_action">
                                                        <ul>
                                                            <li><a href="#" title=" Add to Wishlist "><i class="fa fa-heart"></i></a></li>
                                                            <li><a href="javascript:void(0)" @click="addToCart({{ $book }})" title=" Add to cart "><i class="fa fa-shopping-cart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="quick_view">
                                                        <a href="#" data-toggle="modal" data-target="#modal_box" title="Quick view"><i class="fa fa-search"></i></a>
                                                    </div>

                                                </div>
                                                <div class="product_content">
                                                    <div class="samll_product_ratting">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="small_product_name">
                                                        <a title="{{ $book->title }}" href="{{ url('book/'.$book->slug) }}">{{ $book->title }}</a>
                                                    </div>
                                                    <div class="small_product_price">
                                                        <span class="new_price"> Ksh {{ number_format($book->price,2) }} </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($loop->even)
                                        </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--featured product area end-->
                </div>
                <div class="col-lg-3 col-md-4">
                    <!--arrivals small product start-->
                    <div class="top_sellers featured mb-40">
                        <div class="top_title">
                            <h2>  New arrivals</h2>
                        </div>
                        <div class="small_product_active owl-carousel">
                            <div class="small_product_item">
                                @foreach($latest_books as $latest_book)
                                <div class="small_product">
                                    <div class="small_product_thumb">
                                        <a href="{{ url('book/'.$latest_book->slug) }}">
                                            <img src="{{ url($latest_book->image) }}" alt="{{ $latest_book->title }}">
                                        </a>
{{--                                        <div class="product_discount">--}}
{{--                                            <span>-10%</span>--}}
{{--                                        </div>--}}
                                    </div>
                                    <div class="small_product_content">
                                        <div class="samll_product_ratting">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="small_product_name">
                                            <a title="{{ $latest_book->title }}" href="{{ url('book/'.$latest_book->slug) }}">{{ $latest_book->title }}</a>
                                        </div>
                                        <div class="small_product_price">
                                            <span class="new_price"> KES {{ number_format($latest_book->price,2) }} </span>
{{--                                            <span class="old_price">  $30.50  </span>--}}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
{{--                            <div class="small_product_item">--}}
{{--                                <div class="small_product">--}}
{{--                                    <div class="small_product_thumb">--}}
{{--                                        <a href="single-product.html"><img src="assets/img/cart/cart4.jpg" alt=""></a>--}}
{{--                                        <div class="product_discount">--}}
{{--                                            <span>-10%</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="small_product_content">--}}
{{--                                        <div class="samll_product_ratting">--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_name">--}}
{{--                                            <a title="Printed Summer Dress" href="single-product.html">Printed Dress</a>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_price">--}}
{{--                                            <span class="new_price"> $27.00 </span>--}}
{{--                                            <span class="old_price">  $30.50  </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="small_product">--}}
{{--                                    <div class="small_product_thumb">--}}
{{--                                        <a href="single-product.html"><img src="assets/img/cart/cart10.jpg" alt=""></a>--}}
{{--                                    </div>--}}
{{--                                    <div class="small_product_content">--}}
{{--                                        <div class="samll_product_ratting">--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_name">--}}
{{--                                            <a title="Printed Summer Dress" href="single-product.html"> Summer Dress</a>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_price">--}}
{{--                                            <span class="new_price"> $27.00 </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="small_product">--}}
{{--                                    <div class="small_product_thumb">--}}
{{--                                        <a href="single-product.html"><img src="assets/img/cart/cart8.jpg" alt=""></a>--}}
{{--                                        <div class="product_discount">--}}
{{--                                            <span>-10%</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="small_product_content">--}}
{{--                                        <div class="samll_product_ratting">--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_name">--}}
{{--                                            <a title="Printed Summer Dress" href="single-product.html">Printed Dress</a>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_price">--}}
{{--                                            <span class="new_price"> $27.00 </span>--}}
{{--                                            <span class="old_price">  $30.50  </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="small_product">--}}
{{--                                    <div class="small_product_thumb">--}}
{{--                                        <a href="single-product.html"><img src="assets/img/cart/cart7.jpg" alt=""></a>--}}
{{--                                    </div>--}}
{{--                                    <div class="small_product_content">--}}
{{--                                        <div class="samll_product_ratting">--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_name">--}}
{{--                                            <a title="Printed Summer Dress" href="single-product.html">hanbag justo</a>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_price">--}}
{{--                                            <span class="new_price"> $27.00 </span>--}}
{{--                                            <span class="old_price">  $30.50  </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="small_product">--}}
{{--                                    <div class="small_product_thumb">--}}
{{--                                        <a href="single-product.html"><img src="assets/img/cart/cart8.jpg" alt=""></a>--}}
{{--                                        <div class="product_discount">--}}
{{--                                            <span>-10%</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="small_product_content">--}}
{{--                                        <div class="samll_product_ratting">--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_name">--}}
{{--                                            <a title="Printed Summer Dress" href="single-product.html"> Summer Dress</a>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_price">--}}
{{--                                            <span class="new_price"> $27.00 </span>--}}
{{--                                            <span class="old_price">  $30.50  </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="small_product six">--}}
{{--                                    <div class="small_product_thumb">--}}
{{--                                        <a href="single-product.html"><img src="assets/img/cart/cart9.jpg" alt=""></a>--}}
{{--                                    </div>--}}
{{--                                    <div class="small_product_content">--}}
{{--                                        <div class="samll_product_ratting">--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="index.html#"><i class="fa fa-star"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_name">--}}
{{--                                            <a title="Printed Summer Dress" href="single-product.html">Printed Dress</a>--}}
{{--                                        </div>--}}
{{--                                        <div class="small_product_price">--}}
{{--                                            <span class="new_price"> $27.00 </span>--}}
{{--                                            <span class="old_price">  $30.50  </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <!--arrivals small product end-->
                </div>
            </div>
        </div>
    </div>

    <!--blog area start-->
    <div class="blog_area mb-40">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="top_title mb-30">
                        <h2> Blog Posts</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="blog_active owl-carousel">
                    @foreach($blogs as $blog)
                    <div class="col-lg-3">
                        <div class="single_blog">
                            <div class="blog_thumb">
                                <a href="{{ url('blog/'.$blog->slug) }}"><img src="{{ url($blog->image) }}" alt="{{ $blog->title }}"></a>
                            </div>
                            <div class="blog_content">
                                <h4 class="blog_title"><a href="{{ url('blog/'.$blog->slug) }}">{{ $blog->title }}</a></h4>
                                <p class="blog_desc">
                                {{ substr(strip_tags($blog->description), 0, 100) }}
                                </p>
                                <div class="blog_post">
                                    <ul>
{{--                                        <li class="post_date">{{ $blog->created_at }} 01</li>--}}
                                        <li class="read_more"><a href="{{ url('blog/'.$blog->slug) }}">Read More</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach

                </div>
            </div>
        </div>
    </div>
    <!--blog area end-->

@endsection

