@extends('layouts.frontend')

@section('styles')
    <style>
        .th_column{
            font-weight: 400;
            font-size: 16px;
            color: #000;
        }
        .td_column{
            font-size: 16px;
        }
        input[type="radio"]{width: 15% !important;height: 20px!important;}
        input[type="text"],input[type="date"]{background-color: white !important;}
    </style>


@endsection
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <div class="Checkout_page_section">
        <div class="container">

            <div class="checkout_form">
                <div class="row">

                        @auth()
                        <div class="col-lg-6 col-md-6">
                            <h3>Pickup Information</h3>
                         <div class="card" style="background-color: #faedef; margin-bottom: 20px">
                             <div class="p-1">
                                 <div class="row">
                                     <div class="col-md-6">
                                         <span class="px-5 th_column">Full Name</span>
                                     </div>
                                     <div class="col-md-6 td_column">{{ auth()->user()->name }}</div>
                                 </div>

                                 <div class="row">
                                     <div class="col-md-6">
                                         <span class="px-5 pb-2 th_column">Phone Number</span>
                                     </div>
                                     <div class="col-md-6 td_column">{{ auth()->user()->phone }}</div>
                                 </div>

                             </div>
                         </div>
                          <delivery-component></delivery-component>
                        </div>
                            @else
                        <div class="col-lg-8 col-md-8">

                           <div class="row">
                               <div class="col-6">
                                   <div class="card">
                                       <h3 style="color: #000;text-decoration: underline;font-weight: bold" class="text-center text-uppercase pt-2">Create an Account</h3>

                                       <form id="register"  class="p-2" method="post" action="{{ url('customer-register') }}">
                                           <div class="col-12 mb-10">
                                               <label>Name</label>
                                               <input type="text" name="name" value="" placeholder="Enter Name">
                                           </div>
                                           @csrf
                                           <div class="col-12 mb-10">
                                               <label>Email</label>
                                               <input type="email" name="email" value="" placeholder="Email Address">
                                           </div>

                                           <div class="col-12 mb-10">
                                               <label>Phone</label>
                                               <input type="text" name="phone" value="" placeholder="0712 137 247">
                                           </div>

                                           <div class="col-12 mb-10">
                                               <label>Password</label>
                                               <input type="password" name="password" value="">
                                           </div>

                                           <div class="col-12">
                                               <button type="button" onclick="registerUser()" class="btn btn-primary">Register</button>
                                           </div>
                                       </form>
                                   </div>

                               </div>
                               <div class="col-6" >
                                   <div class="card">
                                        <h3 style="text-decoration: underline;font-weight: bold" class="text-center  pt-2">Login</h3>

                                       <form id="login" style="padding: 20px" method="post" action="{{ url('customer-login') }}">
                                           <div class="col-12 mb-20">
                                               <label> Email</label>
                                               <input type="email" name="login_email" placeholder="Email Address">
                                               @error('email')
                                               <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                               @enderror
                                           </div>
                                            @csrf
                                           <div class="col-12 mb-20">
                                               <label> Password</label>
                                               <input type="password" name="login_password">
                                           </div>
                                           <div class="col-12">
                                               <button type="button" onclick="loginUser()" name="submit" class="btn btn-success">Login</button>
                                           </div>
                                       </form>
                                   </div>

                               </div>
                           </div>
                        </div>
                            @endauth


                        <div class="col-lg-4 col-md-4 offset-1">
                            @auth()
                            <div class="order_form_two">
                                <h3>My order</h3>
                            </div>
                               <checkout-cart></checkout-cart>
                                @endauth
                        </div>

                </div>
            </div>
        </div>
    </div>

    @endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script>

        function loginUser(){
            let email = $('input[name="login_email"]').val();
            let pwd = $('input[name="login_password"]').val();
            if(email == '' || pwd == '')
                toastr.error(`Fill all the required details`);
            const url = "{{ url('customer-login?_token='.csrf_token()) }}";
            let data = {'login_email':email,'login_password':pwd};
            $.post(url,data,function (response) {
               if(response.status)
               {
                   toastr.success("Login has been successful");
                   window.location.href = "{{ url('checkout') }}"
               }
                else
                   toastr.error(`Enter the Correct Login Credentials`);

            });
        }

        function registerUser() {
            // console.log("I have been Hit");
            let email = $('input[name="email"]').val();
            let name = $('input[name="name"]').val();
            let phone = $('input[name="phone"]').val();
            let pwd = $('input[name="password"]').val();
            let url = "{{ url('customer-register?_token='.csrf_token()) }}";

            let data = {'name':name,'phone':phone,'email':email,'password':pwd};
            $.post(url,data).done(function (response) {
                if(response.status){
                    toastr.success("Registration has been successful");
                    window.location.href = "{{ url('checkout') }}"
                }
                // console.log("response status is "+response.status);

            }).fail(function (resp) {
                if (resp.status == 422){
                    toastr.error(`Fill all the required fields with the right details`);
                }
                // console.log(resp);
            })
        }
    </script>
    @endsection
