<div class="small_product_item">
    @foreach($featured_books as $featured_book)
        <div class="small_product">
            <div class="small_product_thumb">
                <a href="{{ url('book/'.$featured_book->slug) }}">
                    <img src="{{ url($featured_book->image) }}" alt="{{ $featured_book->title }}">
                </a>
            </div>
            <div class="small_product_content">
                <div class="samll_product_ratting">
                    <ul>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                    </ul>
                </div>
                <div class="small_product_name">
                    <a title="{{ $featured_book->title }}" href="{{ url('book/'.$featured_book->slug) }}">{{ $featured_book->title }}</a>
                </div>
                <div class="small_product_price">
                    <span class="new_price"> {{ $featured_book->price }} </span>
                    {{--                                            <span class="old_price">  $30.50  </span>--}}
                </div>
            </div>
        </div>
    @endforeach
</div>
