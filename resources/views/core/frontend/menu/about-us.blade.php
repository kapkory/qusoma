@extends('layouts.frontend')

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area bread_about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <div class="breadcrumb_header">
                            <a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                            <span><i class="fa fa-angle-right"></i></span>
                            <span> Portfolio</span>
                        </div>
                        <div class="breadcrumb_title">
                            <h2>About Us</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--about section area -->
    <div class="about_section_aera about_two">
        <div class="container about_container">
            <div class="row no-gutters">
                <div class="col-12">
                    <div class="about_content_two_inner">
                        <div class="about_content about_c_two">
                            <h1>Welcome to Qusoma Library Services</h1>
                            <p>
                                We are an online bookshop and library based in Nairobi, Kenya. We sell books in all formats (including audio books) and we do free deliveries within Nairobi CBD. We also deliver books to any place in Kenya.

                                We also rent books to registered members. Registration fee is Kshs. 2,000/=. A book is rented at Kshs. 100 per week.

                            </p>
                            <p>
                                We help you build your own library. We are an online bookshop and library. We sell and rent books in all formats: paperback, e books and audio books.

                                We do deliveries to any part of Kenya. Delivery within Nairobi CBD is free.
                            </p>
                            <p>
                                Reading is the best investment you can do to yourself. The return on investment is amazing.

                                Look into the habits of the successful and you will find that they are usually great readers. Many successful people attribute the turning point in their lives to picking up a certain book. If you can read about the accomplishments of those you admire, you cannot help but lift your own sights.
                            </p>
                            <p>
                                Anthony Robbins remarked that "Success leaves clues," and reading is one of the best means of absorbing such clues.

                                Curiosity and the capacity to learn are vital for achievement, thus the saying "leaders are readers." The person who seeks growth, Dale Carnegie said, "must soak and tan his mind constantly in the vats of literature."
                            </p>
                        </div>
                        <div class="about__img about_img_two">
                            <img src="{{ url('frontend/assets/img/ship/about3.jpg') }}" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--about section end-->

    <!--counterup area -->
    <div class="counter_up_area mb-50">
        <div class="container-fluid counter">
            <div class="row no-gutters">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single_counterup">
                        <div class="counter_img">
                            <img src="{{ url('frontend/assets/img/cart/count.png') }}" alt="">
                        </div>
                        <div class="counter_info">
                            <h2 class="counter_number">2170</h2>
                            <p>happy customers</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single_counterup count-two">
                        <div class="counter_img">
                            <img src="{{ url('frontend/assets/img/cart/count2.png') }}" alt="">
                        </div>
                        <div class="counter_info">
                            <h2 class="counter_number">1</h2>
                            <p>Nomination</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single_counterup">
                        <div class="counter_img">
                            <img src="{{ url('frontend/assets/img/cart/count3.png') }}" alt="">
                        </div>
                        <div class="counter_info">
                            <h2 class="counter_number">351</h2>
                            <p>Books Rented</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single_counterup count-two">
                        <div class="counter_img">
                            <img src="{{ url('frontend/assets/img/cart/cart5.png') }}" alt="">
                        </div>
                        <div class="counter_info">
                            <h2 class="counter_number">3149</h2>
                            <p>Sold Books</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--counterup end-->
@endsection