@extends('layouts.frontend')

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area contact_bread contact">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <div class="breadcrumb_header">
                            <a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                            <span><i class="fa fa-angle-right"></i></span>
                            <span> Contact</span>
                        </div>
                        <div class="breadcrumb_title">
                            <h2>Contact</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <div class="contact_area mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="contact_message">
                        <div class="contact_title">
                            <h2>Send us a Message</h2>
                        </div>
                        <?php $status = isset($_GET['status']) ? $_GET['status'] : ''; ?>
                        @if($status == 'success')
                            <div class="alert alert-success">
                                We have successfully received your Request, We shall get in Contact as soon as possible
                            </div>
                            @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form id="contact-form" method="POST"  action="{{ url('contact-us') }}">
                            <div class="row">
                                <div class="col-lg-6">
                                    <input name="name" placeholder="Name *" type="text">
                                </div>
                                <div class="col-lg-6">
                                    <input name="email" placeholder="Email *" type="email">
                                </div>
                                <div class="col-lg-6">
                                    <div class="select_form_select">
                                        <select name="subject">
                                            <option name="Request Book">Request Book</option>
                                            <option name="Ask Book">Ask Book</option>
                                            <option name="Rent Book">Rent Book</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <input name="phone" placeholder="Phone *" type="text">
                                </div>
                                @csrf

                                <div class="col-12">
                                    <div class="contact_textarea">
                                        <textarea placeholder="Message *" name="message"  class="form-control2" ></textarea>
                                    </div>
                                    <button type="submit"> Send Message </button>
                                </div>
                                <div class="col-12">
                                    <p class="form-messege"></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="contact_info_wrapper">
                        <div class="contact_title">
                            <h4>contact us</h4>
                        </div>
                        <div class="contact_info mb-15">
                            <ul>
                                <li>
                                    <i class="fa fa-fax"></i>  Location : Nairobi, Kenya
                                </li>
                                <li><i class="fa fa-phone"></i>
                                    <a href="#">0734 465398</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i> info@qusomalib.com
                                </li>
                            </ul>
                        </div>
                        <div class="contact_info mb-15">
                            <h3><strong>Working hours</strong></h3>
                            <p><strong>Monday – Saturday</strong>:  08:00AM – 6:00PM</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
