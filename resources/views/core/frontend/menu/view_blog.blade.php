@extends('layouts.frontend')

@section('content')
    <div class="breadcrumbs_area portfolio_d">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <div class="breadcrumb_header">
                            <a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                            <span><i class="fa fa-angle-right"></i></span>
                            <span>Blog</span>
                        </div>
                        <div class="breadcrumb_title">
                            <h2>Blog </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="main_blog_area port_deatils">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-7">
                    <div class="blog_left_area">
                        <!--portfolio details thumb-->
                        <div class="singli_blog_wrapper gallery port_gallery mb-40">
                            <div class="blog__title mb-30">
                                <h1>{{ $blog->title }}</h1>
                                <div class="blog__post">
                                    <ul>
                                        <li class="post_author">Posts by : Admin</li>
                                        <li class="post_date">
                                        {{ $blog->created_at->isoFormat('MMM Do YY') }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--portfolio details thumb start-->
                            <div class="single_blog">
                                <div class="portfolio_d_thumb">
                                    <a href="#"><img style="max-height: 300px" src="{{ url($blog->image) }}" alt="{{ $blog->title }}"></a>
                                </div>

                                <div class="blog_entry_content port_content">
                                    <p>
                                        {!! $blog->description !!}
                                    </p>

                                </div>
                                <div class="blog_entry_meta port_meta">
                                    <ul>
                                        <li><a href="#">0 comments</a></li>
                                        <li> / Tags: <a href="#">fashion</a></li>
                                    </ul>
                                </div>
                                <div class="blog_social_sharing gallery_social port_social">
                                    <h3>Share this post</h3>
                                    <ul>
                                        <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#" title="Facebook"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#" title="Facebook"><i class="fa fa-pinterest"></i></a></li>
                                        <li><a href="#" title="Facebook"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#" title="Facebook"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--portfolio details thumb start-->
                        </div>

                        <!--relatedposts area start-->
                        <div class="related_post">
                            <h2>Related posts</h2>
                        </div>
                        <!--relatedposts area end-->

                        <!--comments area start-->
                        <div class="comments_area">
                            <div class="comments__title">
                                <h3>Leave a Reply </h3>
                            </div>
                            <div class="comments__notes">
                                <p>Your email address will not be published.</p>
                            </div>
                            <div class="comment__form portfolio_c">
                                <form action="#">
                                    <label for="comment_1">Comment</label>
                                    <textarea name="comment" id="comment_1" cols="30" rows="10"></textarea>
                                    <div class="comment_input_area fix">
                                        <div class="single_comment_input">
                                            <label>Name </label>
                                            <input type="text">
                                        </div>
                                        <div class="single_comment_input middle">
                                            <label>Email </label>
                                            <input type="text">
                                        </div>
                                        <div class="single_comment_input">
                                            <label>Website </label>
                                            <input type="text">
                                        </div>
                                    </div>
                                    <button type="submit">Post Comment</button>
                                </form>
                            </div>
                        </div>
                        <!--comments area end-->

                    </div>
                </div>
                <div class="col-lg-3 col-md-5">
                    <div class="blog_right_sidebar">
                        <div class="widget_search_bar mb-30">
                            <h3>Search</h3>
                            <form action="#">
                                <input placeholder="search.." type="text">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>


                        <div class="widget_tweets small_thumb mb-30">
                            <h3>Recent Posts</h3>
                            <div class="widget_blog_small_thumb">
                             @include('core.frontend.shared.featured_books_sidebar',['featured_books'=>$featured_books])

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
