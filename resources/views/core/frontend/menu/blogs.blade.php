@extends('layouts.frontend')

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area blog_bread">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <div class="breadcrumb_header">
                            <a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                            <span><i class="fa fa-angle-right"></i></span>
                            <span> blog</span>
                        </div>
                        <div class="breadcrumb_title">
                            <h2>blog</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <div class="srrvices_img_area">
        <div class="container">
            <div class="row">
                @foreach($blogs as $blog)
                <div class="col-lg-4 col-md-6">
                    <div class="single_img_services services_three mb-40">
                        <div class="services_thumb">
                            <div class="text-center">
                                <img style="border-radius: 50%" height="180px" src="{{ url($blog->image) }}" alt="{{ $blog->title }}">
                            </div>

                        </div>
                        <div class="services_content">
                            <a href="{{ url('blog/'.$blog->slug) }}"><h3>{{ $blog->title }}</h3></a>
                            <p>
                                {{ substr(strip_tags($blog->description), 0, 100) }}
                            </p>
                            <div class="text-center">
                            <a class="btn btn-outline-primary" style="border-radius: 10px" href="{{ url('blog/'.$blog->slug) }}">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach

            </div>
        </div>
    </div>
@endsection
