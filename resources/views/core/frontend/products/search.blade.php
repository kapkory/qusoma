@extends('layouts.frontend')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4">
            <div class="sidebar_categorie_area">
                <div class="categories_menu categorie_page_menu right_categorie mb-30">
                    <div class="categories_title ca_title_two">
                        <h2 class="categori_toggle">All categories</h2>
                    </div>
                    <div class="categories_menu_inner categorie_sidebar_inner">
                        <ul>
                            <li><a href="#"><i class="fa fa-caret-right"></i> Accessories</a></li>
                        </ul>
                    </div>
                </div>


                <div class="top_sellers top_seller_two featured mb-40">
                    <div class="top_title">
                        <h2> Bestseller</h2>
                    </div>
                    <div class="small_product_active owl-carousel owl-loaded owl-drag">

                        <div class="owl-stage-outer">
                            <div class="owl-stage">

                                @foreach($featured_books as $featured_book)
                                    <div class="owl-item active" style="width: 268px;">
                                        <div class="small_product_item">
                                            <div class="small_product">
                                                <div class="small_product_thumb">
                                                    <a href="{{ url('book/'.$featured_book->slug) }}">
                                                        <img src="{{ url($featured_book->image) }}" alt="{{ $featured_book->title }}">
                                                    </a>
                                                </div>
                                                <div class="small_product_content">
                                                    <div class="samll_product_ratting">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="small_product_name">
                                                        <a title="{{ $featured_book->title }}" href="{{ url('book/'.$featured_book->slug) }}">{{ $featured_book->title }}</a>
                                                    </div>
                                                    <div class="small_product_price">
                                                        <span class="new_price"> {{ number_format($featured_book->price,2) }} </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="col-lg-9 col-md-8">
            <div class="categorie_d_right">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="large" role="tabpanel">
                       <div class="row">
                           @if(count($books) > 0)
                        @foreach($books as $book)
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="single_product categorie">
                                    <div class="product_thumb">
                                        <a href="{{ url('book/'.$book->slug) }}"><img src="{{ url($book->image) }}" alt="{{ $book->title }}"></a>
                                        <div class="product_action">
                                            <ul>
{{--                                                <li><a href="#" title=" Add to Wishlist "><i class="fa fa-heart"></i></a></li>--}}
                                                <li><a href="#" title=" Add to cart "><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="product_content">
                                        <div class="samll_product_ratting">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="small_product_name">
                                            <a title="{{ $book->title }}" href="{{ url('book/'.$book->slug) }}">{{ $book->title }}</a>
                                        </div>
                                        <div class="small_product_price">
                                            <span class="new_price"> KSH {{ number_format($book->price,2) }} </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           @endforeach
                               @else
                               <div class="alert alert-primary">
                                   We currently don't have that book in our stock, Preview Our Wide Catalogue here <a style="border-radius: 10px" href="{{ url('book-list') }}" class="btn btn-outline-success">Our Catalogue</a>
                               </div>
                               @endif

                       </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
