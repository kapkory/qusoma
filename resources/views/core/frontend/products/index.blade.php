@extends('layouts.frontend')

@section('content')


        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="sidebar_categorie_area">
                        <div class="categories_menu categorie_page_menu mb-30">
                            <div class="categories_title ca_title_two">
                                <h2 class="categori_toggle"> All categories</h2>
                            </div>
                            <div class="categories_menu_inner">
                                <ul>
                                    @foreach($categories as $category)
                                        <li><a href="{{ url('book-category/'.$category->slug) }}"><i class="fa fa-caret-right"></i> {{ $category->name }}</a></li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>


{{--                        <div class="product_banner product_banner_shop fix mb-30">--}}
{{--                            <a href="shop-list.html#"><img src="assets/img/banner/banner1.jpg" alt=""></a>--}}
{{--                        </div>--}}
                        <div class="top_sellers top_seller_two featured mb-40">
                            <div class="top_title">
                                <h2> Featured Books</h2>
                            </div>
                            <div class="small_product_active owl-carousel">
                                <div class="small_product_item">
                                    @include('core.frontend.shared.featured_books_sidebar',['featured_books'=>$featured_books])
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="categorie_d_right">

                        <div class="categorie_product_toolbar mb-30">
                            <div class="categorie_product_button">
                                <ul class="nav" role="tablist">
                                    <li>
                                        <a class="active" data-toggle="tab" href="#large" role="tab" aria-controls="large" aria-selected="true"><i class="fa fa-th-large"></i></a>
                                    </li>
                                    <li>
                                        <a  data-toggle="tab" href="#list" role="tab" aria-controls="list" aria-selected="false"><i class="fa fa-th-list"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <form action="#">
                                <label>Sort By</label>
                                <select name="orderby" id="short">
                                    <option selected value="1">Default sorting</option>
                                    <option value="1">Sort by popularity</option>
                                    <option value="1">Sort by average rating</option>
                                    <option value="1">Sort by rating</option>
                                    <option value="1">Sort by price: low to high</option>
                                    <option value="1">Sort by price: high to low</option>
                                    <option value="1">Price: Lowest first</option>
                                    <option value="1">Product Name: A to Z</option>
                                    <option value="1">Reference: Lowest first</option>
                                    <option value="1">Reference: Highest first</option>
                                </select>
                            </form>
                            <p>Showing 1–12 of 46 results</p>
                        </div>


                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="large" role="tabpanel">

                                    <book-list-component url="{{ url('/') }}"></book-list-component>
{{--                                <product-preview-modal></product-preview-modal>--}}
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
{{--    </div>--}}
    <!--categorie details end-->
@endsection
