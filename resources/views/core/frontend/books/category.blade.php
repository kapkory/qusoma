@extends('layouts.frontend')

@section('content')
    <div class="banner_slide_show mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="categories_menu">
                        <div class="categories_title">
                            <h2 class="categori_toggle"> Our categories</h2>
                        </div>
                        <div class="categories_menu_inner">
                            <ul>
                                @foreach($categories as $category)
                                    <li><a href="{{ url('book-category/'.$category->slug) }}"><i class="fa fa-caret-right"></i> {{ $category->name }}</a></li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    @if($books->count() > 0)
                    <div class="row cate_tab_product">

                            @foreach($books as $book)
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="single_product categorie">
                                <div class="product_thumb">
                                    <a href="{{ url('book/'.$book->slug) }}"><img src="{{ url($book->image) }}" alt="{{ $book->title }}"></a>
                                    <div class="product_action">
                                        <ul>
{{--                                            <li><a href="#" title=" Add to Wishlist "><i class="fa fa-heart"></i></a></li>--}}
{{--                                            <li><a href="#" title=" Add to Compare "><i class="fa fa-retweet"></i></a></li>--}}
                                            <li><a href="#" title=" Add to cart "><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                    </div>
{{--                                    <div class="quick_view">--}}
{{--                                        <a href="#" data-toggle="modal" data-target="#modal_box" title="Quick view"><i class="fa fa-search"></i></a>--}}
{{--                                    </div>--}}

                                </div>
                                <div class="product_content">
                                    <div class="samll_product_ratting">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="small_product_name">
                                        <a title="{{ $book->title }}" href="{{ url('book/'.$book->slug) }}">{{ $book->title }}</a>
                                    </div>
                                    <div class="small_product_price">
                                        <span class="new_price">{{ number_format($book->price,2) }} </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach


                    </div>
                        <div class="page_numbers_toolbar">
                            {{ $books->links() }}
                        </div>
                    @else
                        <div class="alert alert-primary">
                            Seems we have sold all books in this category, <a href="{{ url('contact-us') }}" class="btn btn-success">Contact Us</a> so we can get your book detail
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
