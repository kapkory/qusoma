@extends('layouts.frontend')
@section('content')
    <div class="portfolio_section_area">
        <div class="portfolio_tab">
            <div class="container-fluid">
                <div class="row portfolio_gallery" style="position: relative; height: 681.422px;">
                   @foreach($books as $book)
                    <div class="col-lg-3 col-md-4 col-sm-6 gird_item computers general" style="position: absolute; left: 0%; top: 0px;">
                        <div class="single_portfolio_inner">
                            <div class="portfolio_thumb">
                                <a href="{{ url('book/'.$book->slug) }}"><img src="{{ url($book->image) }}" alt=""></a>
                                <div class="portfolio_popup">
                                    <a class="port_popup" href="{{ url($book->image) }}"><i class="fa fa-search"></i></a>
                                </div>
                                <div class="portfolio_link">
                                    <a href="{{ url('book/'.$book->slug) }}"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="portfolio__content">
                                <a href="{{ url('book/'.$book->slug) }}">{{ $book->title }}</a>
                                <span>{{ $book->author }}</span>
                            </div>
                        </div>

                    </div>
                    @endforeach

                </div>
                {{ $books->links() }}
            </div>
        </div>
    </div>
    @endsection
