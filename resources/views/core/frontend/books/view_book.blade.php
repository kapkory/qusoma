@extends('layouts.frontend')

{{--@section('styles')--}}
{{-- --}}
{{--    @endsection--}}

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area bread_about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <div class="breadcrumb_header">
                            <a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                            <span><i class="fa fa-angle-right"></i></span>
                            <span> <a href="{{ url('books') }}">product</a></span>

                            <span><i class="fa fa-angle-right"></i></span>
                            <span> single product</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--product details start-->
    <div class="product_details">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="product_d_tab fix">

                        <div class="tab-content product_d_content">
                            <div class="tab-pane fade show active" id="p_d_tab1" role="tabpanel" >
                                <div class="modal_tab_img">
                                    <a href="#"><img src="{{ url($book->image) }}" alt="{{ $book->title }}"></a>
                                    <div class="product_discount">
                                        <span>New</span>
                                    </div>
                                    <div class="view_img">
                                        <a class="view_large_img" href="{{ url($book->image) }}">View larger <i class="fa fa-search-plus"></i></a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product_d_right">
                        <h1>{{ $book->title }}</h1>

                                <div class="product_reference">
                                    <p>SKU: <span>{{ $book->sku }}</span></p>

                                </div>
                                <div class="product_condition">
                                    <p>By:  <span>{{ $book->author }}</span></p>

                                </div>
                            </div>



                        <div class="product_short_desc">
                            <p>
                                {!! $book->description   !!}
                            </p>
                        </div>
                        <div class="product_in_stock mb-20">
                            <span> In stock </span>
                        </div>

                    </div>


                <div class="col-md-3 col-lg-3">
                    <div style="background-color: rgb(233, 236, 239); padding: 10px">
                        <div class="book_price text-center">
                            {{ 'KES '.number_format($book->price,2) }}
                        </div>
                        <p class="text-muted">Free Pickup Within Nairobi CBD</p>
                        <div class="product_d_quantity mb-20">
{{--                        <form method="post" action="{{ url('book/'.$book->slug) }}">--}}

                            <button @click="addToCart({{ $book }})" type="button" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-shopping-cart"></i> add to cart</button>
{{--                        </form>--}}
                        </div>
                        <p>
                            <img src="{{ url('img/payment-logos.png') }}" alt="Qusoma Payment Logos">
                        </p>
                    </div>
                    <div class="product_d_social mb-40">
                        <ul>
                            <p style="color: green">Share Via </p>
{{--                whatsapp chat            https://api.whatsapp.com/send?phone=254703639703&text=i am interested in this book--}}
                            <li><a target="_blank" href="https://wa.me/?text={{ url()->current() }}" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i> Whatsapp</a></li>
                            <li><a target="_blank" href="http://twitter.com/share?text={{ $book->title.'&url='.url()->current() }}"> <i class="fa fa-twitter"></i> Tweet </a></li>
                            <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?={{ url('book/'.$book->slug) }}"> <i class="fa fa-facebook-f"></i>  Share  </a></li>
                            <li><a class="m-1" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{ url($book->image) }}&amp;title={{ $book->title }}&amp;utm_source=linkedin&amp;utm_medium=social&amp;utm_campaign=share"> <i class="fa fa-linkedin" aria-hidden="true"></i>LinkedIn </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!--product details end-->

{{--    <!--product details tab-->--}}
{{--    <div class="product__details_tab mb-40">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-12 ">--}}
{{--                    <div class="product_details_tab_inner">--}}
{{--                        <div class="product_details_tab_button">--}}
{{--                            <ul class="nav" role="tablist">--}}
{{--                                <li>--}}
{{--                                    <a class="nav-link active" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="tab-content product_details_content">--}}

{{--                            <div class="tab-pane fade show active" id="reviews" role="tabpanel">--}}
{{--                                <div class="product_d_tab_content_inner">--}}
{{--                                    <div class="product_d_tab_content_item">--}}
{{--                                        <div class="samll_product_ratting">--}}
{{--                                            <ul>--}}
{{--                                                <li>Grade </li>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a class="comment_form" href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <strong>Posthemes</strong>--}}
{{--                                        <p>09/07/2018</p>--}}
{{--                                    </div>--}}
{{--                                    <div class="product_d_tab_content_item">--}}
{{--                                        <strong>demo</strong>--}}
{{--                                        <p>That's OK!</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="product_review_form">--}}
{{--                                    <form action="#">--}}
{{--                                        <h2>Add a review </h2>--}}
{{--                                        <p>Your email address will not be published. Required fields are marked </p>--}}
{{--                                        <div class="samll_product_ratting review_rating">--}}
{{--                                            <span>Your rating</span>--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                                <li><a href="#"><i class="fa fa-star"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-12">--}}
{{--                                                <div class="review_form_comment">--}}
{{--                                                    <label for="review_comment">Your review </label>--}}
{{--                                                    <textarea name="comment" id="review_comment" ></textarea>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-lg-6 col-md-6">--}}
{{--                                                <div class="review_form_author">--}}
{{--                                                    <label for="author">Name</label>--}}
{{--                                                    <input id="author"  type="text">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-lg-6 col-md-6">--}}
{{--                                                <div class="review_form_author">--}}
{{--                                                    <label for="email">Email </label>--}}
{{--                                                    <input id="email"  type="text">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <button type="submit">Submit</button>--}}
{{--                                    </form>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!--product details tab end-->--}}


    <!--product_details_single_product-->
    <div class="product_details_s_product mb-40">
        <div class="container">
            <div class="product_details_s_product_inner">
                <div class="top_title p_details mb-10">
                    <h2>  Related Books:</h2>
                </div>
                <div class="row">
                     @foreach($related_books as $related_book)
                        <div class="col-md-3">
                            <div class="single_product categorie">
                                <div class="product_thumb">
                                    <a href="{{ url('book/'.$related_book->slug) }}">
                                        <img src="{{ url($related_book->image) }}" alt="{{ $related_book->title }}">
                                    </a>
{{--                                    <div class="product_action">--}}
{{--                                        <ul>--}}
{{--                                            <li><a href="#" title=" Add to Wishlist "><i class="fa fa-heart"></i></a></li>--}}
{{--                                            <li><a href="#" title=" Add to Compare "><i class="fa fa-retweet"></i></a></li>--}}
{{--                                            <li><a href="#" title=" Add to cart "><i class="fa fa-shopping-cart"></i></a></li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                    <div class="quick_view">--}}
{{--                                        <a href="#" @click="addToCart({{ $book }})" data-toggle="modal" data-target="#modal_box" title="Quick view"><i class="fa fa-search"></i></a>--}}
{{--                                        <button @click="addToCart({{ $related_book }})" type="button" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-shopping-cart"></i></button>--}}

{{--                                    </div>--}}

                                </div>
                                <div class="product_content">
                                    <div class="samll_product_ratting">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="small_product_name">
                                        <a title="{{ $related_book->title }}" href="{{ url('book/'.$related_book->slug) }}">{{ $related_book->title }}</a>
                                    </div>
                                    <div class="small_product_price">
                                        <span class="new_price"> Ksh {{ number_format($related_book->price,2) }} </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                           @endforeach
                </div>
            </div>
        </div>
    </div>

<script>
    // console.log(localStorage.getItem('books'));
</script>
    <!--product_details_single_end-->
    <script type="application/ld+json">
      {
          "@context":"https://schema.org",
          "@type":"Book",
          "name" : "{{ $book->title }}",
          "author": {
          "@type":"Person",
              "name":"{{ $book->author }}"
      },
          "url" : "{{ url('book/'.$book->slug) }}",
          "workExample" : [{
          "@type": "Book",
          "bookFormat": "https://schema.org/Paperback",
          "potentialAction":{
              "@type":"ReadAction",
              "expectsAcceptanceOf":{
                  "@type":"Offer",
                  "Price":"{{ $book->price }}",
                  "priceCurrency":"KES",
                  "availability": "https://schema.org/InStock"
              }
          }
      }]
      }
  </script>
    @endsection
