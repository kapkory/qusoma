@extends('layouts.frontend')

@section('styles')
    <style>
        .sub-total  {
            line-height: 1.1;
            font-size: 18px;
            font-weight: 600;
            color: #cf142b;
        }
        .books_basket:nth-child(odd) {
            background-color: #dee2e6;
        }
        .hide_title{
            border-bottom: 3px solid #ea3a3c;
        }
    </style>
    <style>
        @media
        only screen and (max-width: 575px) {
            input[type=number]{
                width: 80px;
            }
            .hide_title{
                display: none;
            }
        }
    </style>
    @endsection
@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area contact_bread">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb_content">
                            <div class="breadcrumb_header">
                                <a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                                <span><i class="fa fa-angle-right"></i></span>
                                <span> Shopping Cart</span>
                            </div>
                            <div class="breadcrumb_title">
                                <h2>Shopping Cart</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!--shopping cart area start -->
        <!--shopping cart area end -->
        <div class="shopping_cart_details">
            <div class="container">
              <preview-cart-component></preview-cart-component>
{{--            </div>--}}


{{--        </div>--}}
{{--        <div class="container">--}}
{{--            <form action="cart.html#">--}}
                <!--coupon code area start-->

{{--            </form>--}}
        </div>
    </div>
    <!-- accont area end -->
    @endsection
