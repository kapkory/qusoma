@extends('layouts.customer')
@section('page_title')
    Dashboard
    @endsection
@section('customer_content')
<p>From your account dashboard. you can easily check &amp; view your <a href="#">recent orders</a>, manage your <a href="#">shipping and billing addresses</a> and <a href="#">Edit your password and account details.</a></p>
@endsection
