@extends('layouts.customer')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    #order{{ $order->id }}
@endsection

@section('title') orders @endsection

@section('customer_content')
    @isset($_GET['payment'])
        @if($_GET['payment'] == 'success')
            <div class="alert alert-success">
                You Payment is being Processed, we are working on fulfilling your order.
            </div>
            @endif
        @endisset
    <link href="{{ url('codefox/plugins/sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    @if($order->status = \App\Repositories\StatusRepository::getOrderStatus('pending'))
        <a href="{{ url('customer/orders/choose-payment/'.$order->id) }}" style="color: black; border-radius: 10px" class="btn btn-outline-success float-right">Pay All</a>
    @endif
    @include('common.bootstrap_table_ajax',[
        'table_headers'=>["id","books.title"=>"title","quantity","unit_price","action"],
        'data_url'=>'customer/orders/order/'.$order->id.'/list',
        'base_tbl'=>'order_books'
        ])
    @endsection
@include('common.auto_modal',[
       'modal_id'=>'book_modal',
       'modal_title'=>'BOOK FORM',
       'modal_content'=>Form::autoForm(["quantity","form_model"=>\App\Models\Core\OrderBook::class],"customer/orders/order/update")
   ])
@section('scripts')
    <script src="{{ asset('codefox/assets/js/jquery.history.js') }}"></script>
    <script src="{{ url('codefox/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ url('codefox/assets/js/jquery.datetimepicker.js') }}"></script>

    @include('common.javascript')

    @endsection
