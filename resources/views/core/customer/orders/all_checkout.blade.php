@extends('layouts.customer')
@section('styles')
    <style>
        @media only screen and (max-width: 600px) {
            .hide_title {
                display: none;
            }
        }
        input[type="radio"]{width: 15% !important;height: 20px!important;}

    </style>
    @endsection
@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Payment
@endsection

@section('title') Payment @endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h4>Order contents
                </h4>
                <div  class="col-md-12 "><!---->
                    <div class="row hide_title" style="border-bottom: 1px solid grey;">
                        <div class="col-sm-7">
                            <h2  class="h4">Books </h2>
                        </div>

                        <div class="col-sm-3">
                            <p class="h4 text-center">Quantity</p>
                        </div>
                        <div  class="col-sm-2">
                            <p  class="h4 align-left">Sub-total</p>
                        </div>

                    </div>
                    <?php $total_cost = 0; $delivery = 0;?>
                    @foreach($orders as $order)
                        <?php
                        $book_cost = $order->price * $order->quantity;
                        $total_cost +=$book_cost;
                        if ($order->shipping_address_id)
                            $delivery = 1;
                        ?>

                    <div class="books_basket">
                        <div  class="row p-2">
                            <div  class="col-sm-2 ">
                                <a  href="{{ url('book/'.$order->slug) }}">
                                    <img  src="{{ url($order->image) }}" alt="Into Abba`s Arms - Sandra Wilson" height="80">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <h4 >{{ $order->title }}</h4>
                            </div>

                            <div class="col-sm-2">
                                <input readonly  min="0" max="100" name="book_count" value="{{ $order->quantity }}" type="text">
                            </div>
                            <div class="col-sm-2">
                                <span class="sub-total">KES {{ number_format($book_cost,2) }}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="coupon_code_area pt-3">
                        <div  class="row">
                            <div  class="col-lg-6 col-md-6">
                                <div  class="coupon_code coupon">

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div  class="coupon_code">
                                    <div class="cart_total_amount">
                                        <div  class="cart_subtotal order">
                                            <p  class="order_total">Total</p>
                                            <p  class="order_amount">Ksh {{ number_format($total_cost,2) }} </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h4>Delivery</h4>

                <div>
                    @if($delivery == 0)
                       <div class="alert alert-info">
                           Pick in Town
                       </div>
                        @else
                        @if(auth()->user()->lastDeliveryAddress())
                            <?php $address = auth()->user()->lastDeliveryAddress(); ?>
                        <div class="alert alert-info">
                            <table class="table-responsive">
                                <tr>
                                    <th>Town</th>
                                    <td>{{ $address->town }}</td>
                                </tr>
                                <tr>
                                    <th>Postal Code</th>
                                    <td>{{ $address->postal_code }}</td>
                                </tr>
                                <tr>
                                    <th>Address Line 1</th>
                                    <td>&nbsp;&nbsp;{{ $address->address_1 }}</td>
                                </tr>
                                <tr>
                                    <th>Address Line 2</th>
                                    <td>&nbsp;&nbsp;{{ $address->address_2 }}</td>
                                </tr>

                            </table>
                        </div>
                        @endif

                        @endif

                    <div  class="coupon_code">
                        <div class="cart_total_amount">
                            @if($delivery == 1)
                                <div class="cart_subtotal order" style="padding: 1px !important;">
                                    <p  class="order_total">Delivery Charge</p>
                                    <p  class="order_amount">Ksh 400 </p>
                                </div>
                            @endif
                            <div class="cart_subtotal order">
                                <p  class="order_total">Total</p>
                                <p  class="order_amount">Ksh @if($delivery == 1) {{ number_format($total_cost + 400,2) }} @else{{ number_format($total_cost,2) }} @endif</p>
                            </div>
                            <form method="post" action="{{ url('customer/orders/order/'.$order_id.'/pay-order') }}">
                                @csrf
                                <input type="hidden" name="method" value="{{ $payment_method }}">
                                <div class="cart_to_checkout">
                                    <input style="width: 40%" class="btn btn-success" type="submit" name="submit" value="Pay Now">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
