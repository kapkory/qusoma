@extends('layouts.customer')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Payment Method
@endsection

@section('title') Payment Method @endsection

@section('customer_content')

<div class="row">
    <div class="col-md-4 payment">
       <div class="card">
        <div class="card-body">
            <a href="{{ url('customer/orders/checkout/mpesa/'.$order_id) }}">
                <div class="img"><img src="{{ url('frontend/assets/img/mpesa.png') }}" class="img-responsive"></div>
                <h2 class="h4 text-black pt-2">Pay using M-PESA</h2>
                <p class="text-black">Use this option to pay by Safaricom M-Pesa.</p>
            </a>
        </div>
       </div>
    </div>

{{--    <div class="col-md-4 payment">--}}
{{--       <div class="card">--}}
{{--           <div class="card-body">--}}
{{--               <a href="{{ url('customer/orders/checkout/pesapal/'.$order_id) }}" >--}}
{{--                   <div class="img"><img src="{{ url('frontend/assets/img/pesapal.png') }}" class="img-responsive"></div>--}}
{{--                   <h2 class="h4 text-black pt-2">Pay by Pesapal</h2>--}}

{{--                   <p class="text-black">Use this option to pay with <strong>Visa</strong>, <strong>MasterCard</strong>, <strong>American Express</strong> and other mobile money services.</p>--}}

{{--               </a>--}}
{{--           </div>--}}
{{--       </div>--}}
{{--    </div>--}}

    <div class="col-md-4 payment">
        <div class="card">
            <div class="card-body">
                <a href="{{ url('customer/orders/checkout/paypal/'.$order_id) }}">
                    <div class="img"><img src="{{ url('frontend/assets/img/paypal.png') }}" class="img-responsive"></div>
                    <h2 class="h4 text-black pt-2">Pay using Paypal</h2>

                    <p class="text-black">Use this option to pay by Paypal.</p>

                </a>
            </div>
        </div>
    </div>
</div>



    @endsection
