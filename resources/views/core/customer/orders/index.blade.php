@extends('layouts.customer')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    Orders
@endsection

@section('title') Orders @endsection

@section('customer_content')
    <link href="{{ url('codefox/plugins/sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css" />


    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["Id","pickup_date","orders.shipping_address_id"=>"shipping_address","cost","action"],
    'data_url'=>'customer/orders/list',
    'base_tbl'=>'orders'
    ])

@endsection

@section('scripts')
    <script src="{{ asset('codefox/assets/js/jquery.history.js') }}"></script>
    <script src="{{ url('codefox/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
{{--    <script src="{{ url('codefox/assets/js/jquery.datetimepicker.js') }}"></script>--}}

{{--    @include('common.javascript')--}}
<script>
    $(function () {
        $('.nav-link').removeClass('active');
        $('.client_view_orders').addClass('active');
    })

</script>
@endsection


