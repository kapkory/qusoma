@extends('layouts.admin')

@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    ShippingAddresses
@endsection

@section('title') ShippingAddresses @endsection

@section('content')
<a href="#shippingaddress_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD SHIPPINGADDRESS</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","county_id","address_1","address_2","postal_code","town","order_id","action"],
    'data_url'=>'customer/address/list',
    'base_tbl'=>'shippingaddresses'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'shippingaddress_modal',
        'modal_title'=>'SHIPPINGADDRESS FORM',
        'modal_content'=>Form::autoForm(\App\Models\Core\ShippingAddress::class,"customer/address")
    ])
@endsection

<script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".tooltip").tooltip("hide");
 </script>