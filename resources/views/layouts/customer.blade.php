@extends('layouts.frontend')
@section('styles')
    <style>
        .nice-select {
            height: 15px !important;
        }
    </style>
    @endsection
@section('script')
    <script src="{{ url('codefox/assets/js/jquery.datetimepicker.js') }}"></script>
@endsection
@section('content')
    <section class="main_content_area my_account">
        <div class="container">
            <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                            <ul role="tablist" class="nav flex-column dashboard-list">
                                @if(isset($real_menus))

                                    @foreach($real_menus as $menu)
                                        <li>
                                            <a href="{{ url($menu->menus->url)  }}"  class="nav-link {{ $menu->slug }} {{ ($loop->first) ? 'active' : '' }}">
                                                <i class="fi-{{ $menu->menus->icon }}"></i>
                                                <span>{{ $menu->menus->label }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                        @endif
                                <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"class="nav-link">logout</a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
                            <div class="tab-pane active" id="dashboard">
                                <h3>@yield('page_title') </h3>
                                @yield('customer_content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @endsection
