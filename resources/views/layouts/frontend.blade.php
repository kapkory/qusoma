<!Doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ isset($meta_title) ? $meta_title : 'Browse a Wide Catalogue of Religious, Africana, Philosophy and other books at Qusoma Library, Nairobi ' }}</title>
    <meta name="description" content="{{ isset($meta_description) ? $meta_description : 'Browse a Wide Catalogue of Religious, Africana, Philosophy and other books at Qusoma Library, We do free deliveries in Nairobi and do deliveries to other parts of the country at a cost. Qusomalib is the leading book shop in town' }}">

    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url()->current() }}">
{{--    <meta property="og:image" content="https://textbookcentre.com/static/img/logo.png">--}}
    <meta property="og:site_name" content="Qusoma Bookshop ">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <meta property="og:title" content="{{ isset($meta_title) ? $meta_title : 'Browse a Wide Catalogue of Religious, Africana, Philosophy and other books at Qusoma Library, Nairobi ' }}" />
    <meta property="og:description" content="{{ isset($meta_description) ? $meta_description : 'Browse a Wide Catalogue of Religious, Africana, Philosophy and other books at Qusoma Library, We do free deliveries in Nairobi and do deliveries to other parts of the country at a cost. Qusomalib is the leading book shop in town' }}" />
    <!-- all css here -->
    <link rel="stylesheet" href="{{ url('frontend/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/assets/css/bundle.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/assets/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/assets/css/responsive.css') }}">
    <script src="{{ url('frontend/assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <script src="{{ url('codefox/assets/js/jquery.min.js') }}"></script>


{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>--}}
{{--    <script src="{{ url('frontend/assets/js/vendor/jquery-1.12.0.min.js') }}"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.5/vue.min.js" defer></script>--}}
    <meta name="_token" content="{{ csrf_token() }}">
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    @yield('styles')

    <style>
        .xoo-cp-atcn {
            overflow: auto;
            padding: 5px;
            border: 1px solid transparent;
            border-radius: 4px;
            display: block;
            text-align: left;
        }

        span .xoo-cp-close {
            font-size: 15px;
            position: absolute;
            padding-right: 15px;
            top: -11px;
            background-color: #fff;
            border-radius: 50%;
            cursor: pointer;
        }
        .cart_book_price{
            color: #1dacd6;
            font-weight: 600;
            line-height: 26px;
        }
        .book_image image{
            height: 20px !important;
        }
        .modal-dialog{
            position: relative;
            overflow-y: auto;
            top: 15%;
            overflow-x: auto;
            width: 60% !important;
        }
        .book_price{
            font-weight: 600;
            font-size: 29px;
            line-height: 32px;
            margin-bottom: 3px;
            color: #5cb85c;
        }
        .prod_btn{
            color: white !important;
            border-radius: 20px
        }

    </style>


@yield('script')
    <script src="{{ asset('codefox/assets/js/jquery.history.js') }}"></script>
    <script src="{{ asset('codefox/assets/js/jquery.history.js') }}"></script>

    <script src="{{ url('codefox/assets/js/jquery.form.js') }}"></script>
</head>
<body>
<!-- Add your site or application content here -->
<!--header area start-->
<div class="header_area" >

    <!--header top start-->
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="top_left_sidebar">
                        <div class="contact_link">
                            <span>Call us : <strong>0734 465398</strong></span>
                        </div>
                        <div class="switcher">
                            <ul>
                                <li class="currency">
                                    <a href="{{ url('/') }}">Currency : <strong>KSH </strong></a>
                                </li>
                                <li class="languages"><a href="{{ url('/') }}"><img src="{{ url('frontend/assets/img/logo/lion.jpg') }}" alt=""> English </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="header_right_info text-right">
                        <ul>
                            @auth()
                            <li class="my_account"><a href="{{ url(auth()->user()->role) }}"><i class="fa fa-user" aria-hidden="true"></i> My account</a></li>
                            @endauth
{{--                            <li class="my_wishlist"><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>My wishlist</a></li>--}}
                            <li class="link_checkout"><a href="#"><i class="fa fa-check" aria-hidden="true"></i> checkout </a></li>
                            @guest()
                            <li class="log_in"><a href="{{ url('login') }}"><i class="fa fa-lock" aria-hidden="true"></i> Log in  </a></li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--header middel-->
    <div class="header_middle" >
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="logo">
                        <a href="{{ url('/') }}"><img src="{{ url('frontend/assets/img/logo/logo.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="search_form">
                        <form action="{{ url('search') }}#">
                            <input name="s" placeholder="Search book by Title or Author ..." type="text">
                            <div class="select_categories">
                                <select name="select" id="categorie">
                                    <option selected value="1">All Categories</option>
                                </select>
                            </div>
                            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3" id="testss">
                  <cart-component checkout-url="{{ url('book-basket') }}"></cart-component>
                </div>
            </div>
        </div>
    </div>
    <!--header bottom start-->
    <div class="header_bottom sticky-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="main_menu_inner">
                        <div class="main_menu d-none d-lg-block">
                            <nav>
                                <ul>
                                    <li><a href="{{ url('/') }}">Home </a>
                                    </li>
                                    <li class="dropdown_item">
                                        <a href="{{ url('book-list') }}">Book List </a>
                                    </li>
                                    <li class="dropdown_item">
                                        <a href="{{ url('gallery') }}">Browse By Images</a>
                                    </li>
                                    <li class="dropdown_item">
                                        <a href="{{ url('blogs') }}">Blogs </a>
                                    </li>
                                    <li><a href="{{ url('about-us') }}">About Us</a></li>
                                    <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>

                        <div class="mobile-menu portfolio_mobail about d-lg-none">
                            <nav>
                                <ul>
                                    <li>
                                        <a href="{{ url('/') }}">Home</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('book-list') }}">Book List</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('gallery') }}">Gallery</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blogs') }}">Blog</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('about-us') }}">About Us</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('contact-us') }}">Contact Us</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--header area end-->
<!--categorie details start-->
<div class="categorie_details" id="app" data-site_url="{{ url('/') }}">
    @yield('content')
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="close_modal float-right">
                    <span class="xoo-cp-close close" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></span>
                </div>
                <div class="alert alert-success" style="margin: 10px">
                    <div  class="xoo-cp-atcn" id="exampleModalLabel">
                        <i class="fa fa-check"></i> Book has been added successfully
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2 offset-1">
                            <div class="book_image">
                                <img height="100" src="" alt="Book Image">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-blue modal-title">Book Title</h4>
                        </div>

                        <div class="col-md-3">
                            <div class="cart_book_price">
                                KSH 250
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('book-list') }}" type="button"  class="btn btn-secondary prod_btn">Continue Shopping</a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="{{ url('book-basket') }}" type="button" class="btn btn-success prod_btn">View Cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <added-to-cart-component ref="modal"></added-to-cart-component>--}}
</div>


<!--newsletter area start-->
<div class="newsletter_area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-2 col-md-6">
                <div class="footer_logo">
                    <a href="#"><h5 class="text-white">Qusoma Library Services</h5></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="social_icone">
                    <ul>
                        <li><a href="https://www.facebook.com/qusomalibrary/" title="facebook" data-toggle="tooltip" data-placement="top"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/qusomalibrary" title="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://ke.linkedin.com/in/qusoma-library-b0b634177" title="linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="{{ url('/') }}#" title="feed"><i class="fa fa-feed"></i></a></li>
                        <li><a href="{{ url('/') }}#" title="pinterest"><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="newslatter_inner fix">
                    <h4>send Newsletters</h4>
                    <form action="{{ url('/') }}#">
                        <input placeholder="enter your email" type="text">
                        <button type="submit">Subscribe</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--newsletter area end-->

<!--footer area start-->
<div class="footer_area">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single_footer">
                        <h4>store information</h4>

                        <ul>
                            <li><i class="fa fa-home"></i> Nairobi, Kenya</li>
                            <li><i class="fa fa-phone"></i> 07 34 465 398</li>
                            <li><a href="{{ url('/') }}#"><i class="fa fa-envelope-square"></i> info@qusomalib.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single_footer">
                        <h4>Information</h4>
                        <ul>
                            <li><a href="{{ url('/') }}#"><i class="fa fa-circle"></i> Our Blog</a></li>
                            <li><a href="{{ url('/') }}#"><i class="fa fa-circle"></i> About Our Shop</a></li>
                            <li><a href="{{ url('/') }}#"><i class="fa fa-circle"></i> Secure Shopping</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single_footer">
                        <h4>My account</h4>
                        <ul>
                            <li><a href="{{ url('/') }}#"><i class="fa fa-circle"></i> My orders</a></li>
                            <li><a href="{{ url('/') }}#"><i class="fa fa-circle"></i> About Us</a></li>
                            <li><a href="{{ url('/') }}#"><i class="fa fa-circle"></i> Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single_footer">
                        <h4>instagram</h4>
                        <div class="instagram_img">
                            <div class="single_instagram_img">
                                <a href="{{ url('/') }}#"><img src="assets/img/instagram/instagram1.jpg" alt=""></a>
                            </div>
                            <div class="single_instagram_img">
                                <a href="{{ url('/') }}#"><img src="assets/img/instagram/instagram2.jpg" alt=""></a>
                            </div>
                            <div class="single_instagram_img">
                                <a href="{{ url('/') }}#"><img src="assets/img/instagram/instagram3.jpg" alt=""></a>
                            </div>
                            <div class="single_instagram_img">
                                <a href="{{ url('/') }}#"><img src="assets/img/instagram/instagram4.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright_area">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="widget_copyright">
                        <p> Copyright &copy; {{ date('Y') }} <a href="{{ url('/') }}#">QusomaLib</a>. &nbsp;&nbsp;&nbsp;developed by <span style="color: brown">CassavaHub</span></p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="payment">
                        <a href="{{ url('/') }}#"><img src="{{ url('frontend/assets/img/visha/payment.png') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--footer area end-->

<!-- modal area start -->


{{--<script>--}}
{{--    import ProductPreviewModal from "../../js/components/ProductPreviewModal";--}}
{{--    export default {--}}
{{--        components: {ProductPreviewModal}--}}
{{--    }--}}
{{--</script>--}}
<!-- modal area end -->
<script type="application/ld+json">
    {
    "@context":"https:\/\/schema.org",
    "@type":"Organization",
    "@id":"{{ url('/') }}#organization",
    "name":"Qusoma Library Service",
    "url":"{{ url('/') }}",
    "sameAs":
    [
    "https://www.facebook.com/qusomalibrary/",
    "https://twitter.com/qusomalibrary?lang=en",
    "https://ke.linkedin.com/in/qusoma-library-b0b634177"
    ],
    "email":"info@qusomalib.com",
    "logo":"{{ url('frontend/assets/img/logo/lion.jpg') }}",
    "contactPoint":[
    {
    "@type":"ContactPoint",
    "telephone":"0734 465398",
    "contactType":"customer support"
    }]
    }
</script>
@yield('scripts')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });
    });
</script>
<script src="{{ asset('js/app.js') }}" ></script>

<script src="{{ url('frontend/assets/js/popper.js') }}"></script>

<script src="{{ url('frontend/assets/js/plugins.js') }}"></script>

<!-- all js here -->
{{--<script src="{{ url('frontend/assets/js/bootstrap.min.js') }}"></script>--}}
{{--<script src="{{ url('js/owlCarousel.js') }}"></script>--}}
<script>
    // $(function () {

    $('.slider_active').owlCarousel({
        animateOut: 'fadeOut',
        autoplay: true,
        loop: true,
        nav: false,
        autoplayTimeout: 8000,
        items: 1,
        dots:true,
    });
    // })
</script>
<script src="{{ url('frontend/assets/js/main.js') }}" ></script>
</body>
</html>

