@if(Request::ajax())
    @if(isset($_GET['t_optimized']))
        @yield('t_optimized')
    @elseif(isset($_GET['ta_optimized']))
        @yield('ta_optimized')
    @else
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <h4 class="page-title">@yield('title')</h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{ url(auth()->id()) }}">Dashboard</a>
                                    </li>
                                    {{--                                <li>--}}
                                    {{--                                    <a href="page-starter.html#">Pages</a>--}}
                                    {{--                                </li>--}}
                                    <li class="active">
                                        Current
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-body">
                                @yield('content')
                            </div>
                        </div><!-- end col -->
                    </div>
                    <!-- end row -->


                </div> <!-- container-fluid -->

            </div> <!-- content -->



        </div>
    @endif
    @include('common.essential_js')
@else
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Qusoma Library </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

{{--    <meta content="Qusoma Library Services" name="description" />--}}
    <meta content="Qusoma" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ url('codefox/assets/images/favicon.ico') }}">
    <link href="{{ url('codefox/plugins/sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="{{ url('codefox/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('codefox/assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('codefox/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('codefox/assets/css/style.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ url('codefox/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <script src="{{ url('codefox/assets/js/jquery.min.js') }}"></script>
{{--    <script src="{{ url('js/ckeditor.js') }}"></script>--}}
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>

    <script src="{{ url('codefox/assets/js/modernizr.min.js') }}"></script>
    <script src="{{ asset('codefox/assets/js/jquery.history.js') }}"></script>
    <script src="{{ url('codefox/assets/js/jquery.datetimepicker.js') }}"></script>
    @stack('header-scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
{{--    <link href="{{ url('sweetalert/dist/sweetalert.css') }}" rel="stylesheet">--}}
    <script src="{{ url('codefox/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ url('codefox/assets/js/jquery.form.js') }}"></script>
</head>


<body>
@include('common.javascript')
<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <!--<a href="index.html" class="logo"><span>Code<span>Fox</span></span><i class="mdi mdi-layers"></i></a>-->
            <!-- Image logo -->
            @auth()
            <a href="{{ url(auth()->user()->role) }}" class="logo">
                        <span>
                            <img src="{{ url('codefox') }}/assets/images/logo.png" alt="" height="20">
                        </span>
                <i>
                    <img src="{{ url('codefox') }}/assets/images/logo_sm.png" alt="" height="28">
                </i>
            </a>
                @endauth
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container-fluid">

                <!-- Navbar-left -->
                <ul class="nav navbar-left">
                    <li>
                        <button type="button" class="button-menu-mobile open-left waves-effect">
                            <i class="dripicons-menu"></i>
                        </button>
                    </li>

                    <li class="d-none d-sm-block lang-option">
                        <select class="selectpicker form-control" title="" data-width="110px">
                            <option> English </option>
                        </select>
                    </li>
                </ul>

                <!-- Right(Notification) -->
                <ul class="nav navbar-right list-inline">
                    <li class="d-none d-sm-block list-inline-item">
                        <form role="search" class="app-search">
                            <input type="text" placeholder="Search..."
                                   class="form-control">
                            <a href="#"><i class="fa fa-search"></i></a>
                        </form>
                    </li>
                    <li class="list-inline-item">
                        <div class="dropdown">
                            <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                                <i class="dripicons-bell"></i>
                                <span class="badge badge-pill badge-pink">4</span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right dropdown-lg user-list notify-list">
                                <li class="list-group notification-list m-b-0">
                                    <div class="slimscroll">
                                        <!-- list item-->
                                        <a href="javascript:void(0);" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left p-r-10">
                                                    <em class="fa fa-diamond bg-primary"></em>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                                    <p class="m-0">
                                                        There are new settings available
                                                    </p>
                                                </div>
                                            </div>
                                        </a>

                                        <!-- list item-->
                                        <a href="javascript:void(0);" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left p-r-10">
                                                    <em class="fa fa-cog bg-warning"></em>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="media-heading">New settings</h5>
                                                    <p class="m-0">
                                                        There are new settings available
                                                    </p>
                                                </div>
                                            </div>
                                        </a>

                                        <!-- list item-->
                                        <a href="javascript:void(0);" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left p-r-10">
                                                    <em class="fa fa-bell-o bg-custom"></em>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="media-heading">Updates</h5>
                                                    <p class="m-0">
                                                        There are <span class="text-primary font-600">2</span> new updates available
                                                    </p>
                                                </div>
                                            </div>
                                        </a>

                                        <!-- list item-->
                                        <a href="javascript:void(0);" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left p-r-10">
                                                    <em class="fa fa-user-plus bg-danger"></em>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="media-heading">New user registered</h5>
                                                    <p class="m-0">
                                                        You have 10 unread messages
                                                    </p>
                                                </div>
                                            </div>
                                        </a>

                                        <!-- list item-->
                                        <a href="javascript:void(0);" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left p-r-10">
                                                    <em class="fa fa-diamond bg-primary"></em>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                                    <p class="m-0">
                                                        There are new settings available
                                                    </p>
                                                </div>
                                            </div>
                                        </a>

                                        <!-- list item-->
                                        <a href="javascript:void(0);" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left p-r-10">
                                                    <em class="fa fa-cog bg-warning"></em>
                                                </div>
                                                <div class="media-body">
                                                    <h5 class="media-heading">New settings</h5>
                                                    <p class="m-0">
                                                        There are new settings available
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                                <!-- end notification list -->
                            </ul>
                        </div>
                    </li>

                    <li class="dropdown user-box list-inline-item">
                        <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                            <img src="{{ url('codefox/assets/images/users/avatar-1.jpg') }}" alt="user-img" class="rounded-circle user-img">
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                            <li><a href="javascript:void(0)" class="dropdown-item">Profile</a></li>
{{--                            <li><a href="javascript:void(0)" class="dropdown-item"><span class="badge badge-pill badge-info float-right">4</span>Settings</a></li>--}}
{{--                            <li><a href="javascript:void(0)" class="dropdown-item">Lock screen</a></li>--}}
                            <li class="dropdown-divider"></li>
                            <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            </li>
                        </ul>
                    </li>

                </ul> <!-- end navbar-right -->

            </div><!-- end container-fluid -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="slimscroll-menu" id="remove-scroll">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">
                    <li class="menu-title">Navigation</li>
                    @if(isset($real_menus))

                        @foreach($real_menus as $menu)

                            @if($menu->type=='single' && @$menu->menus)
                                <li>
                                    <a href="{{ url($menu->menus->url)  }}" class="load-page">
                                        <i class="fi-{{ $menu->menus->icon }}"></i>
                                        <span>{{ $menu->menus->label }}</span>
                                    </a>
                                </li>
                            @endif

                            @if($menu->type=='many')
                                    <li>
                                        <a href="javascript: void(0);"><i class="fi-{{ $menu->icon }}"></i><span> {{ $menu->label }} </span> <span class="menu-arrow"></span></a>
                                        <ul class="nav-second-level nav" aria-expanded="false">
                                            @foreach($menu->children as $drop)
                                                @if($drop->label)
                                            <li><a class="load-page" href="{{ url($drop->url) }}">{{ $drop->label }}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                            @endif

                        @endforeach
                    @endif


                </ul>

            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            @auth()
                            <h4 class="page-title"> @yield('title',ucwords(auth()->user()->role).' Page') </h4>
                            @endauth
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="{{ url(auth()->id()) }}">Dashboard</a>
                                </li>
{{--                                <li>--}}
{{--                                    <a href="page-starter.html#">Pages</a>--}}
{{--                                </li>--}}
                                <li class="active">
                                    Current
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-sm-12">
                       <div class="card">
                           <div class="card-body">
                               @yield('content')
                           </div>
                       </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->


            </div> <!-- container-fluid -->

        </div> <!-- content -->

        <footer class="footer">
             {{ date('Y') }} © Qusoma Library Services. <span class="d-none d-sm-inline-block"> - CassavaHub.co.ke</span>
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>
{{--<script src="{{ url('sweetalert/dist/sweetalert.min.js') }}"></script>--}}

<!-- jQuery  -->
<script src="{{ url('codefox/assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('codefox/assets/js/metisMenu.min.js') }}"></script>
<script src="{{ url('codefox/assets/js/waves.js') }}"></script>
<script src="{{ url('codefox/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ url('codefox/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>



@yield('scripts')
<!-- App js -->
<script src="{{ url('codefox/assets/js/jquery.core.js') }}"></script>
<script src="{{ url('codefox/assets/js/jquery.app.js') }}"></script>

</body>
</html>
    @endif
