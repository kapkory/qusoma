/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
// require('./owlCarousel');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('book-list-component', require('./components/BookListComponent.vue').default);
Vue.component('preview-cart-component', require('./components/PreviewCartComponent.vue').default);
Vue.component('checkout-cart', require('./components/CheckoutCart.vue').default);
Vue.component('delivery-component', require('./components/DeliveryComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
let username =  {
    props: ['name'],
    template: '<p>Hi @{{ name }}</p>'
};
import { addToCartMixin } from './addToCartMixin.js';
import CartComponent from './components/CartComponent.vue';


const app = new Vue({
    el: '#app',
    data:{
      carts:[]
    },
    mixins: [ addToCartMixin ],
    methods: {

    },
    components:{
        'username':username
    }
});
const app2 = new Vue({
    el: '#testss',
    data:{
        // site_url:
    },
    components:{
        'cart-component':CartComponent
    }
});

