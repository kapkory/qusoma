export const addToCartMixin = {
   // created(){
   //     console.log('Example Mixin by levi')
   // },
    data:function(){
       return{
           carts:[],
           book:{},
           site_url : 'http://localhost/qusoma/public/'
       }
    },
    methods:{
        addToCart: function(book) {
            // $('#exampleModal').modal('show');
            this.book = book;
            self = this;
            $('#exampleModal').on('show.bs.modal', function (event) {

                // var button = $(event.relatedTarget) // Button that triggered the modal
                var modal = $(this);
                let image = "<a href='"+self.site_url+'/'+'book/'+book.slug+"'> <img height=\"100\" alt='"+book.title+"'  src='"+self.site_url+'/'+book.image+"'></a>"
                modal.find('.book_image').html(image);
                modal.find('.modal-title').text(book.title);
                modal.find('.cart_book_price').text('Ksh '+book.price)
            });
            let data = { id: book.id, 'title':book.title, 'price':book.price, 'image':this.site_url+book.image, quantity:1 };
            let carts = this.carts;
            // carts.filter(cart=>cart.id != book.id)
            // console.log('we reached here');
            let result = carts.filter(function (cart) {
                return cart.id == book.id;
            });
            // console.log(result);
            if (result.length == 0){
                this.carts.push(data);
                let products = [];
                let bks = localStorage.getItem('books');
                if (!bks){
                    products.push(data);
                    localStorage.setItem('books',JSON.stringify(products))
                }
                else{
                    let mybooks = JSON.parse(bks);
                    products = mybooks;
                    products.push(data);
                    localStorage.setItem('books',JSON.stringify(products))
                }
                $('.cart__quantity').text(products.length);
            }

        }
    }
}
