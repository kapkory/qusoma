export const UpdateShippingMixin = {
    data: function() {
        return {
            shipping_cost:0,
        }
    },
    methods:{

        proceedToPayment:function () {
            let delivery = $('input[name="delivery"]').val();
            let site_url = $('#app').data('site_url');
            if (delivery == 1){
             let pickupdate = $('input[name="pickup_date"]').val();

             if (pickupdate)
             {
                 let books = localStorage.getItem('books');
                 let data = {'method':1,'pickup_date':pickupdate,'books':books};
                 const url = site_url+'/customer/orders';
                 $.post(url,data,function (response) {
                     localStorage.removeItem('books');
                     // console.log(response);
                     // return false;
                   window.location.href = response.redirect;
                 }).fail(function (response) {
                     toastr.error('An error occurred, Enter Correct Details')
                 });
                 // console.log('Pickup date is '+pickupdate);
             }
                else{
                 $("input[name='pickup_date']").trigger('click');
                 toastr.error(`Set a Pick Up Date`);
             }

            }else if(delivery == 2){
                let county = $('select[name="counties"]').val();
                // console.log('county us '+county);
                let town = $('input[name="town"]').val();
                let address_1 = $('input[name="address_line_1"]').val();
                let address_2 = $('input[name="address_line_2"]').val();
                let postal_address = $('input[name="postal_address"]').val();
                if (town == '' || address_1== '' || postal_address =='')
                    toastr.error(`Please Fill all the required details`)

                let books = localStorage.getItem('books');
                let data = {'county':county,'town':town,'address_1':address_1,'address_2':address_2,'postal_address':postal_address,'books':books,'method':2};
                const url = site_url+'/customer/orders';
                $.post(url,data,function (response) {
                    localStorage.removeItem('books');
                    window.location.href = response.redirect;
                }).fail(function (response) {
                    toastr.error('An error occurred, Enter Correct Details')
                });
            }
         // console.log('delivery_method is '+ );
         // console.log('Date is '+);
         // console.log($('#shipping_address_form').serialize());
            // console.log('Shipping Cost is '+localStorage.getItem('shipping_cost') );
            // console.log('Delivery Method is '+this.delivery_method )
            // let delivery = $('')
        }
    }
};
